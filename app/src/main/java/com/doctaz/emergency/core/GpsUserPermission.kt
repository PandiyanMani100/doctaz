package com.doctaz.emergency

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.IntentSender.SendIntentException
import android.content.pm.PackageManager
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.doctaz.emergency.constant.AppConstant.chatPageRequestCode
import com.doctaz.emergency.constant.AppConstant.gpsRequestCode

import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import dagger.hilt.android.qualifiers.ActivityContext
import javax.inject.Inject


class GpsUserPermission @Inject constructor(@ActivityContext private val context: Context) {
    private var isConnected: Boolean = false
    private var mSettingsClient: SettingsClient? = null
    private var mLocationSettingsRequest: LocationSettingsRequest? = null
    private var locationManager: LocationManager? = null
    private var locationRequest: LocationRequest? = null
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var gps: Boolean = false

    fun gpsStatus(): Boolean {
        locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        gps = locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)
        return gps
    }


    init {
        mSettingsClient = LocationServices.getSettingsClient(context)
        locationRequest = LocationRequest.create()
        locationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest!!.interval = 10 * 1000.toLong() // 10 seconds
        locationRequest!!.fastestInterval = 2 * 1000.toLong() // 2 seconds
        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest!!)
        mLocationSettingsRequest = builder.build()
        //**************************
        builder.setAlwaysShow(true) //this is the key ingredient
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
    }

    fun enableGPS() {
        mSettingsClient!!.checkLocationSettings(mLocationSettingsRequest)
            .addOnSuccessListener(context as Activity) { //  GPS is already enable, callback GPS status through listener
                makeToast(/*context.resources.getString(R.string.location_enabled)*/"")
            }
            .addOnFailureListener(context) { e: Exception ->
                val statusCode = (e as ApiException).statusCode
                when (statusCode) {
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {
                        // Show the dialog by calling startResolutionForResult(), and check the result in onActivityResult().
                        val result = e as ResolvableApiException
                        result.startResolutionForResult(context, gpsRequestCode)
                    } catch (sie: SendIntentException) {
                        Log.i(context.toString(), "PendingIntent unable to execute request.")
                    }
                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                        makeToast(/*context.resources.getString(R.string.settings_change_unavailable_errorMessage)*/"")
                        Log.e(
                            context.toString(),
                            /*context.resources.getString(R.string.settings_change_unavailable_errorMessage)*/""
                        )
                    }
                }
            }
    }

    private fun makeToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun locationPermission(): Boolean {
        val permissionResult1 = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
        val permissionResult2 = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
        return permissionResult1 == PackageManager.PERMISSION_GRANTED && permissionResult2 == PackageManager.PERMISSION_GRANTED
    }

    fun cameraPermission(): Boolean {
        val permissionResult = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA)
        return permissionResult == PackageManager.PERMISSION_GRANTED
    }


    fun readExternalStoragePermission(): Boolean {
        val permissionResult = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE)
        return permissionResult == PackageManager.PERMISSION_GRANTED
    }

    fun writeExternalStoragePermission(): Boolean {
        val permissionResult = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        return permissionResult == PackageManager.PERMISSION_GRANTED
    }

    fun requestUserPermission() {
        // Creating String Array with Permissions.
        ActivityCompat.requestPermissions(
            context as Activity,
            arrayOf(Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE), chatPageRequestCode)
    }

    // checking internet connection
    fun isConnectedInternet(): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val capabilities = connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null)
                isConnected = capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) || capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
            else
                return isConnected
        } else {
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            isConnected = activeNetworkInfo != null && activeNetworkInfo.isConnected
        }
        return isConnected
    }

}