package com.doctaz.emergency.firebase

import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.util.Log
import com.doctaz.emergency.view.activity.SessionManager

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import org.json.JSONException
import org.json.JSONObject

class MyFirebaseMessagingService : FirebaseMessagingService() {
    private lateinit var sessionManager: SessionManager

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Log.d("FirebaseToken ==>", token)
        sessionManager= SessionManager(this)
        sessionManager.setFirebaseTokenId(token);
    }

    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)
    }

}