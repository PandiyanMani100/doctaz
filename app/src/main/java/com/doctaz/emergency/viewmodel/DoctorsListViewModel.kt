package com.doctaz.emergency.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.doctaz.emergency.model.CreateInboxGroupResponse
import com.doctaz.emergency.model.DoctorsListResponse
import com.doctaz.emergency.model.DoctorsTypeResponse
import com.doctaz.emergency.repository.DoctorsListRepository
import com.doctaz.emergency.utils.Resource


class DoctorsListViewModel @ViewModelInject constructor(private var doctorsListRepository: DoctorsListRepository) : ViewModel() {
    private var loginResponse = MutableLiveData<Resource<DoctorsListResponse>>()
    private var inboxId = MutableLiveData<Resource<CreateInboxGroupResponse>>()
    private var doctorsType = MutableLiveData<Resource<DoctorsTypeResponse>>()

    fun getLoginResponse(): MutableLiveData<Resource<DoctorsListResponse>> {
        loginResponse = doctorsListRepository.getDoctorListResponse()
        return loginResponse
    }

    fun getdoctorsType(): MutableLiveData<Resource<DoctorsTypeResponse>> {
        doctorsType = doctorsListRepository.getdoctorsType()
        return doctorsType
    }

    fun createInboxBoxGroupId(userId:String, userType:String,receiverId:String,receiverType:String):MutableLiveData<Resource<CreateInboxGroupResponse>>{
        val requestData=HashMap<String,String?>()
        requestData["user_id"]=userId
        requestData["user_type"]=userType
        requestData["recipient_id"]=receiverId
        requestData["recipient_type"]=receiverType
        inboxId=doctorsListRepository.getInboxGroupId(requestData)
        return inboxId
    }


}
