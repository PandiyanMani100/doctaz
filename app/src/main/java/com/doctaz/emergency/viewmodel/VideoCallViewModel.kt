package com.doctaz.emergency.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.doctaz.emergency.model.VideoCallResponse
import com.doctaz.emergency.model.WalletsResponse
import com.doctaz.emergency.repository.VideoCallingRepository
import com.doctaz.emergency.utils.Resource

import okhttp3.ResponseBody

class VideoCallViewModel @ViewModelInject constructor(private var videoCallingRepository: VideoCallingRepository) :ViewModel() {
    private var videoCallData = MutableLiveData<Resource<VideoCallResponse>>()
    private var addUserData = MutableLiveData<Resource<ResponseBody>>()
    private var walletsResponse = MutableLiveData<Resource<WalletsResponse>>()

    fun getVideoCallResponse(senderId:String,receiverId:String,senderType:String,receiverType:String,call_minutes:String,call_amount:String): MutableLiveData<Resource<VideoCallResponse>> {
        val requestData = HashMap<String, String?>()
        requestData["sender_id"] = senderId
        requestData["receiver_id"] = receiverId
        requestData["sender_type"] = senderType
        requestData["receiver_type"] = receiverType
        requestData["call_minutes"] = call_minutes
        requestData["call_amount"] = call_amount
        videoCallData = videoCallingRepository.getVideoCallResponse(requestData)
        return videoCallData
    }
    fun getAddUserResponse(email:String,mobile:String,message: String,roomNumber:String): MutableLiveData<Resource<ResponseBody>> {
        val requestData = HashMap<String, String?>()
        requestData["email_address"] = email
        requestData["phone_number"] = mobile
        requestData["custom_message"] = message
        requestData["room_number"] = roomNumber
        addUserData = videoCallingRepository.getAddUserResponse(requestData)
        return addUserData
    }

    fun geWalletsdetailsResponse(user_id: String, user_type: String): MutableLiveData<Resource<WalletsResponse>> {
        val requestData = HashMap<String, String?>()
        requestData["user_id"] = user_id
        requestData["user_type"] = user_type

        walletsResponse = videoCallingRepository.getWalletsDetailsResponse(requestData)
        return walletsResponse
    }

    fun getvideocallparams(receiver_id:String,receiver_type:String): MutableLiveData<Resource<ResponseBody>> {
        val requestData = HashMap<String, String?>()
        requestData["receiver_id"] = receiver_id
        requestData["receiver_type"] = receiver_type

        addUserData = videoCallingRepository.incomingCall(requestData)
        return addUserData
    }





}