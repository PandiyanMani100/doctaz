package com.doctaz.emergency.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.doctaz.emergency.model.CallHistory
import com.doctaz.emergency.model.VideoCallResponse
import com.doctaz.emergency.repository.CallRepository
import com.doctaz.emergency.utils.Resource
import okhttp3.ResponseBody

class CallViewModel @ViewModelInject constructor(private var callRepository: CallRepository) :ViewModel() {
    private var videoCallData = MutableLiveData<Resource<VideoCallResponse>>()
    private var addUserData = MutableLiveData<Resource<ResponseBody>>()
    private var walletsResponse = MutableLiveData<Resource<CallHistory>>()

 /*   fun getVideoCallResponse(senderId:String,receiverId:String,senderType:String,receiverType:String): MutableLiveData<Resource<VideoCallResponse>> {
        val requestData = HashMap<String, String?>()
        requestData["sender_id"] = senderId
        requestData["receiver_id"] = receiverId
        requestData["sender_type"] = senderType
        requestData["receiver_type"] = receiverType
        videoCallData = callRepository.getVideoCallResponse(requestData)
        return videoCallData
    }
    fun getAddUserResponse(email:String,mobile:String,message: String,roomNumber:String): MutableLiveData<Resource<ResponseBody>> {
        val requestData = HashMap<String, String?>()
        requestData["email_address"] = email
        requestData["phone_number"] = mobile
        requestData["custom_message"] = message
        requestData["room_number"] = roomNumber
        addUserData = callRepository.getAddUserResponse(requestData)
        return addUserData
    }*/

    fun getCallHistorySenderResponse(sender_type:String,sender_id:String): MutableLiveData<Resource<CallHistory>> {
        val requestData = HashMap<String, String?>()
        requestData["sender_id"] = sender_id
        requestData["sender_type"] = sender_type

        walletsResponse = callRepository.getCallHistorySenderResponse(requestData)
        return walletsResponse
    }

    fun getCallHistoryReceiverResponse(sender_type:String,sender_id:String): MutableLiveData<Resource<CallHistory>> {
        val requestData = HashMap<String, String?>()
        requestData["receiver_id"] = sender_id
        requestData["receiver_type"] = sender_type

        walletsResponse = callRepository.getCallHistoryReciverResponse(requestData)
        return walletsResponse
    }


}