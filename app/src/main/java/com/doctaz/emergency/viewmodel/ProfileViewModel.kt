package com.doctaz.emergency.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.doctaz.emergency.model.ProfileResponse
import com.doctaz.emergency.repository.ProfileRepository
import com.doctaz.emergency.utils.Resource

class ProfileViewModel @ViewModelInject constructor(val profileRepository: ProfileRepository) : ViewModel() {
    private var profileData = MutableLiveData<Resource<ProfileResponse>>()

    fun getProfileData(): MutableLiveData<Resource<ProfileResponse>> {
        profileData = profileRepository.getProfileData()
        return profileData
    }
}