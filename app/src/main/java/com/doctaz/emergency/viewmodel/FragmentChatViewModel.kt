package com.doctaz.emergency.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.doctaz.emergency.model.RecentMessageModel
import com.doctaz.emergency.repository.FragmentChatRepository
import com.doctaz.emergency.room.ChatTable


class FragmentChatViewModel @ViewModelInject constructor(private var fragmentChatRepository: FragmentChatRepository) : ViewModel() {
    private var chatHistory = MutableLiveData<List<ChatTable>>()
    private var recentChatList = MutableLiveData<List<RecentMessageModel>>()

    fun insertData(chat: ArrayList<ChatTable>) {
        fragmentChatRepository.insertData(chat)
    }

    fun getUniqueGroup(): MutableLiveData<List<RecentMessageModel>> {
        recentChatList = fragmentChatRepository.getUniqueGroup()
        return recentChatList
    }

    fun makeChatReadStatus(groupId: String) {
        fragmentChatRepository.makeMessagesReadStatus(groupId)
    }


}