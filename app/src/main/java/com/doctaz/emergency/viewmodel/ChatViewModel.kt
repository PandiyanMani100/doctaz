package com.doctaz.emergency.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.doctaz.emergency.model.RecentMessageResponse
import com.doctaz.emergency.repository.ChatRepository
import com.doctaz.emergency.room.ChatTable
import com.doctaz.emergency.utils.Resource

import okhttp3.ResponseBody


class ChatViewModel @ViewModelInject constructor(private val chatRepository: ChatRepository) : ViewModel() {
    private var chatArrayList = MutableLiveData<List<ChatTable>>()
    private var sendMessage = MutableLiveData<Resource<ResponseBody>>()
    private var recentChat = MutableLiveData<Resource<RecentMessageResponse>>()

    fun insertData(chat: ArrayList<ChatTable>) {
        chatRepository.insertData(chat)
    }

    fun getChatHistory(groupId: String): MutableLiveData<List<ChatTable>> {
        chatArrayList = chatRepository.getChatHistory(groupId)
        return chatArrayList
    }

    fun sendMessage(senderId: String, senderType: String, messageGroupId: String, message: String, file: String): MutableLiveData<Resource<ResponseBody>> {
        val requestData = HashMap<String, String?>()
        requestData["sender_id"] = senderId
        requestData["sender_type"] = senderType
        requestData["message_group_id"] = messageGroupId
        requestData["message"] = message
        requestData["file"] = file
        sendMessage = chatRepository.sendMessage(requestData)
        return sendMessage
    }

    fun makeChatReadStatus(groupId:String){
        chatRepository.makeMessagesReadStatus(groupId)
    }

    fun getRecentChatHistory(userId: String, userType: String): MutableLiveData<Resource<RecentMessageResponse>> {
        val requestData = HashMap<String, String?>()
        requestData["user_id"] = userId
        requestData["user_type"] = userType
        requestData["status"] = ""
        recentChat = chatRepository.getChatHistory(requestData)
        return recentChat
    }


}