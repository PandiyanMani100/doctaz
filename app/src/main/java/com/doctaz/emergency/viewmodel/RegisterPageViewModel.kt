package com.doctaz.emergency.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.doctaz.emergency.model.DoctorRegistrationResponse
import com.doctaz.emergency.model.NurseRegistrationResponse
import com.doctaz.emergency.model.PatientRegistrationResponse
import com.doctaz.emergency.model.WalletsResponse
import com.doctaz.emergency.repository.RegisterPageRepository
import com.doctaz.emergency.utils.Resource


class RegisterPageViewModel @ViewModelInject constructor(var registerPageRepository: RegisterPageRepository) : ViewModel() {
    private var patientResponse = MutableLiveData<Resource<PatientRegistrationResponse>>()
    private var nurseResponse = MutableLiveData<Resource<NurseRegistrationResponse>>()
    private var walletsResponse = MutableLiveData<Resource<WalletsResponse>>()
    private var doctorResponse = MutableLiveData<Resource<DoctorRegistrationResponse>>()

    fun getPatientRegistrationResponse(name: String, email: String, password: String, address: String, phone: String, sex: String, dob: String, age: String, bloodGroup: String): MutableLiveData<Resource<PatientRegistrationResponse>> {
        val requestData = HashMap<String, String?>()
        requestData["name"] = name
        requestData["email"] = email
        requestData["password"] = password
        requestData["address"] = address
        requestData["phone"] = phone
        requestData["sex"] = sex
        requestData["birth_date"] = dob
        requestData["age"] = age
        requestData["blood_group"] = bloodGroup
        patientResponse = registerPageRepository.getPatientRegistrationResponse(requestData)
        return patientResponse
    }

    fun getNurseRegistrationResponse(name: String, email: String, password: String, address: String, phone: String, registrationNumber: String, approvedStatus: String): MutableLiveData<Resource<NurseRegistrationResponse>> {
        val requestData = HashMap<String, String?>()
        requestData["name"] = name
        requestData["email"] = email
        requestData["password"] = password
        requestData["address"] = address
        requestData["phone"] = phone
        requestData["registration_certification_number"] = registrationNumber
        requestData["approval_status"] = approvedStatus
        nurseResponse = registerPageRepository.getNurseRegistrationResponse(requestData)
        return nurseResponse
    }





    fun getDoctorRegistrationResponse(name: String, email: String, password: String, address: String, phone: String, registrationNumber: String,department:String,profile:String,fbLink:String,twitterLink:String,googleLink:String,linkedIn:String,approvedStatus: String): MutableLiveData<Resource<DoctorRegistrationResponse>> {
        val requestData = HashMap<String, String?>()
        requestData["name"] = name
        requestData["email"] = email
        requestData["password"] = password
        requestData["address"] = address
        requestData["phone"] = phone
        requestData["profile"] = profile
        requestData["facebook"] = fbLink
        requestData["twitter"] = twitterLink
        requestData["google_plus"] = googleLink
        requestData["linkedin"] = linkedIn
        requestData["approval_status"] = approvedStatus
        requestData["registration_certification_number"] = registrationNumber

        doctorResponse = registerPageRepository.getDoctorRegistrationResponse(requestData)
        return doctorResponse
    }

}