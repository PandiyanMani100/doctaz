package com.doctaz.emergency.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import com.doctaz.emergency.repository.DepartmentFragmentRepository

class DepartmentFragmentViewModel @ViewModelInject constructor(var departmentFragmentRepository: DepartmentFragmentRepository) :ViewModel() {

    fun deleteChatTable() {
        departmentFragmentRepository.deleteChatTable()
    }
}