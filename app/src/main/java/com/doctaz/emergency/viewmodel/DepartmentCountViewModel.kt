package com.doctaz.emergency.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.doctaz.emergency.model.DashboardMainPojo
import com.doctaz.emergency.repository.DepartmentCountRepository
import com.doctaz.emergency.utils.Resource


class DepartmentCountViewModel @ViewModelInject constructor(private var departmentCountRepository: DepartmentCountRepository) : ViewModel() {
    private var departmentCountResponse = MutableLiveData<Resource<DashboardMainPojo>>()

    fun getDepartmentCount(user_type:String ): MutableLiveData<Resource<DashboardMainPojo>> {
        val requestData = HashMap<String, String?>()
        requestData["user_type"] =user_type
        departmentCountResponse = departmentCountRepository.getDepartmentcountResponse(requestData)
        return departmentCountResponse
    }





}
