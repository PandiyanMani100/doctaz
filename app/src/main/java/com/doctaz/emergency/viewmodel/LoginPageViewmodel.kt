package com.doctaz.emergency.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.doctaz.emergency.model.LoginResponse
import com.doctaz.emergency.model.RecentMessageResponse
import com.doctaz.emergency.repository.LoginRepository
import com.doctaz.emergency.room.ChatTable
import com.doctaz.emergency.utils.Resource


class LoginPageViewmodel @ViewModelInject constructor(private var loginRepository: LoginRepository) : ViewModel() {

    fun getLoginResponse(email: String, password: String, fcmId: String): LiveData<Resource<LoginResponse>> {
        val requestData = HashMap<String, String?>()
        requestData["email"] = email
        requestData["password"] = password
        requestData["device_token"] = fcmId
        return loginRepository.getLoginResponse(requestData)
    }

    fun getRecentChatHistory(userId: String, userType: String): LiveData<Resource<RecentMessageResponse>> {
        val requestData = HashMap<String, String?>()
        requestData["user_id"] = userId
        requestData["user_type"] = userType
        requestData["status"] = ""
        return loginRepository.getChatHistory(requestData)
    }


    fun insertData(chat: ArrayList<ChatTable>) {
        loginRepository.insertData(chat)
    }
}