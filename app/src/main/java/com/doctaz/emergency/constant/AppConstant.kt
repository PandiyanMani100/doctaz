package com.doctaz.emergency.constant

object AppConstant {
    val locationRequestCode = 100
    const val gpsRequestCode = 101
    const val appInitialRequestCode = 102
    const val cameraRequestCode=103
    const val galleryRequestCode=104
    const val chatPageRequestCode=105
    const val medicalDocumentRequestCode=105
}