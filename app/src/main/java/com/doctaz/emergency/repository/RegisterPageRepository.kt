package com.doctaz.emergency.repository

import androidx.lifecycle.MutableLiveData
import com.doctaz.emergency.model.DoctorRegistrationResponse
import com.doctaz.emergency.model.NurseRegistrationResponse
import com.doctaz.emergency.model.PatientRegistrationResponse
import com.doctaz.emergency.model.WalletsResponse
import com.doctaz.emergency.retrofit.NetworkApi
import com.doctaz.emergency.utils.Resource
import retrofit2.Call
import retrofit2.Response
import javax.inject.Inject

class RegisterPageRepository @Inject constructor(var networkApi: NetworkApi) {
    private var patientResponse = MutableLiveData<Resource<PatientRegistrationResponse>>()
    private var nurseResponse = MutableLiveData<Resource<NurseRegistrationResponse>>()
    private var walletResponse= MutableLiveData<Resource<WalletsResponse>>()
    private var doctorResponse = MutableLiveData<Resource<DoctorRegistrationResponse>>()


    fun getPatientRegistrationResponse(requestData: HashMap<String, String?>): MutableLiveData<Resource<PatientRegistrationResponse>> {
        networkApi.registerPatient(requestData).enqueue(object : retrofit2.Callback<PatientRegistrationResponse> {
            override fun onResponse(call: Call<PatientRegistrationResponse>, response: Response<PatientRegistrationResponse>) {
                patientResponse.postValue(Resource.success(response.body()))
            }

            override fun onFailure(call: Call<PatientRegistrationResponse>, t: Throwable) {
                patientResponse.postValue(Resource.error(t.toString(), null))
            }
        })

        return patientResponse
    }

    fun getNurseRegistrationResponse(requestData: HashMap<String, String?>): MutableLiveData<Resource<NurseRegistrationResponse>> {
        networkApi.registerNurse(requestData).enqueue(object : retrofit2.Callback<NurseRegistrationResponse> {
            override fun onResponse(call: Call<NurseRegistrationResponse>, response: Response<NurseRegistrationResponse>) {
                nurseResponse.postValue(Resource.success(response.body()))
            }

            override fun onFailure(call: Call<NurseRegistrationResponse>, t: Throwable) {
                nurseResponse.postValue(Resource.error(t.toString(), null))
            }
        })

        return nurseResponse
    }








    fun getDoctorRegistrationResponse(requestData: HashMap<String, String?>): MutableLiveData<Resource<DoctorRegistrationResponse>> {
        networkApi.registerDoctor(requestData).enqueue(object : retrofit2.Callback<DoctorRegistrationResponse> {
            override fun onResponse(call: Call<DoctorRegistrationResponse>, response: Response<DoctorRegistrationResponse>) {
                doctorResponse.postValue(Resource.success(response.body()))
            }

            override fun onFailure(call: Call<DoctorRegistrationResponse>, t: Throwable) {
                doctorResponse.postValue(Resource.error(t.toString(), null))
            }
        })

        return doctorResponse
    }

}