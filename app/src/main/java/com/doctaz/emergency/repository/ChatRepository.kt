package com.doctaz.emergency.repository

import android.app.Application
import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.doctaz.emergency.model.RecentMessageResponse
import com.doctaz.emergency.retrofit.NetworkApi
import com.doctaz.emergency.room.ChatDatabase
import com.doctaz.emergency.room.ChatTable
import com.doctaz.emergency.utils.Resource

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import javax.inject.Inject

class ChatRepository @Inject constructor(var networkApi: NetworkApi, var context:Application) {
    private lateinit var chatDatabase: ChatDatabase
    private var sendMessage=MutableLiveData<Resource<ResponseBody>>()
    private var chatMutableLiveData = MutableLiveData<List<ChatTable>>()
    private var recentChat = MutableLiveData<Resource<RecentMessageResponse>>()

    fun insertData(chat:ArrayList<ChatTable>) {
        chatDatabase = ChatDatabase.getChatDatabase(context)
        CoroutineScope(Dispatchers.IO).launch {
            chatDatabase.chatDao().addChat(chat)
        }
    }

    fun getChatHistory(groupId:String): MutableLiveData<List<ChatTable>> {
        chatDatabase = ChatDatabase.getChatDatabase(context)
        CoroutineScope(Dispatchers.IO).launch {
            chatMutableLiveData.postValue(chatDatabase.chatDao().getUniqueChatList(groupId))
        }
        return chatMutableLiveData
    }

    fun makeMessagesReadStatus(groupId: String) {
        chatDatabase = ChatDatabase.getChatDatabase(context)
        CoroutineScope(Dispatchers.IO).launch {
            chatDatabase.chatDao().makeChatReadStatus(groupId)
        }
    }

    fun sendMessage(requestData: HashMap<String, String?>): MutableLiveData<Resource<ResponseBody>> {
        networkApi.sendMessage(requestData).enqueue(object : retrofit2.Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                sendMessage.postValue(Resource.success(response.body()))
            }
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                sendMessage.postValue(Resource.error(t.toString(),null))
            }
        })
        return sendMessage
    }

    fun getChatHistory(requestData: HashMap<String, String?>): MutableLiveData<Resource<RecentMessageResponse>> {
        networkApi.getChatHistory(requestData).enqueue(object : retrofit2.Callback<RecentMessageResponse> {
            override fun onResponse(call: Call<RecentMessageResponse>, response: Response<RecentMessageResponse>) {
                recentChat.postValue(Resource.success(response.body()))
            }

            override fun onFailure(call: Call<RecentMessageResponse>, t: Throwable) {
                recentChat.postValue(Resource.error(t.toString(), null))
            }
        })
        return recentChat
    }

}