package com.doctaz.emergency.repository

import androidx.lifecycle.MutableLiveData
import com.doctaz.emergency.model.CreateInboxGroupResponse
import com.doctaz.emergency.model.DoctorsListResponse
import com.doctaz.emergency.model.DoctorsTypeResponse
import com.doctaz.emergency.retrofit.NetworkApi
import com.doctaz.emergency.utils.Resource
import retrofit2.Call
import retrofit2.Response
import javax.inject.Inject

class DoctorsListRepository @Inject constructor(private var networkApi: NetworkApi) {
    private var loginResponse = MutableLiveData<Resource<DoctorsListResponse>>()
    private var inboxId = MutableLiveData<Resource<CreateInboxGroupResponse>>()
    private var doctorsType = MutableLiveData<Resource<DoctorsTypeResponse>>()

    fun getDoctorListResponse(): MutableLiveData<Resource<DoctorsListResponse>> {
        networkApi.getDoctorsList().enqueue(object : retrofit2.Callback<DoctorsListResponse> {
            override fun onResponse(call: Call<DoctorsListResponse>, response: Response<DoctorsListResponse>) {
                loginResponse.postValue(Resource.success(response.body()))
            }

            override fun onFailure(call: Call<DoctorsListResponse>, t: Throwable) {
                loginResponse.postValue(Resource.error(t.toString(), null))
            }
        })
        return loginResponse
    }

    fun getInboxGroupId(requestData: HashMap<String, String?>): MutableLiveData<Resource<CreateInboxGroupResponse>> {
        networkApi.createInboxGroup(requestData).enqueue(object : retrofit2.Callback<CreateInboxGroupResponse> {
            override fun onResponse(call: Call<CreateInboxGroupResponse>, response: Response<CreateInboxGroupResponse>) {
                inboxId.postValue(Resource.success(response.body()))
            }

            override fun onFailure(call: Call<CreateInboxGroupResponse>, t: Throwable) {
                inboxId.postValue(Resource.error(t.toString(), null))
            }
        })
        return inboxId
    }


    fun getdoctorsType(): MutableLiveData<Resource<DoctorsTypeResponse>> {
        networkApi.getDoctorsType().enqueue(object : retrofit2.Callback<DoctorsTypeResponse> {
            override fun onResponse(call: Call<DoctorsTypeResponse>, response: Response<DoctorsTypeResponse>) {
                doctorsType.postValue(Resource.success(response.body()))
            }

            override fun onFailure(call: Call<DoctorsTypeResponse>, t: Throwable) {
                doctorsType.postValue(Resource.error(t.toString(), null))
            }
        })
        return doctorsType
    }
}