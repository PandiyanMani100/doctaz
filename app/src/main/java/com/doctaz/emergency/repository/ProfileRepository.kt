package com.doctaz.emergency.repository

import androidx.lifecycle.MutableLiveData
import com.doctaz.emergency.constant.Iconstant.Companion.getProfileDataApi
import com.doctaz.emergency.model.ProfileResponse
import com.doctaz.emergency.retrofit.NetworkApi
import com.doctaz.emergency.utils.Resource
import com.doctaz.emergency.view.activity.SessionManager
import retrofit2.Call

import retrofit2.Response
import javax.inject.Inject

class ProfileRepository @Inject constructor(val networkApi: NetworkApi, val sessionManager: SessionManager) {
    private var profileData = MutableLiveData<Resource<ProfileResponse>>()

    fun getProfileData(): MutableLiveData<Resource<ProfileResponse>> {
        val url = StringBuilder(getProfileDataApi).append(sessionManager.getLoginType()).append("/").append(sessionManager.getLoginId()).toString()
//        val url = StringBuilder(getProfileDataApi).append(sessionManager.getLoginType()).append("/").append(0).toString()
        networkApi.getProfileData(url).enqueue(object : retrofit2.Callback<ProfileResponse> {
            override fun onResponse(call: Call<ProfileResponse>, response: Response<ProfileResponse>) {
                profileData.postValue(Resource.success(response.body()))
            }

            override fun onFailure(call: Call<ProfileResponse>, t: Throwable) {
                profileData.postValue(Resource.error(t.toString(), null))
            }

        })
        return profileData
    }
}