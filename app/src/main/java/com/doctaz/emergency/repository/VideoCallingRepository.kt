package com.doctaz.emergency.repository

import androidx.lifecycle.MutableLiveData
import com.doctaz.emergency.model.VideoCallResponse
import com.doctaz.emergency.model.WalletsResponse
import com.doctaz.emergency.retrofit.NetworkApi
import com.doctaz.emergency.utils.Resource
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import javax.inject.Inject

class VideoCallingRepository @Inject constructor(private var networkApi: NetworkApi) {
    private var videoCallData = MutableLiveData<Resource<VideoCallResponse>>()
    private var addUserData = MutableLiveData<Resource<ResponseBody>>()
    private var incomingCall = MutableLiveData<Resource<ResponseBody>>()
    private var walletResponse = MutableLiveData<Resource<WalletsResponse>>()

    fun getVideoCallResponse(requestData: HashMap<String, String?>): MutableLiveData<Resource<VideoCallResponse>> {
        networkApi.getVideoCall(requestData).enqueue(object : retrofit2.Callback<VideoCallResponse> {
            override fun onResponse(call: Call<VideoCallResponse>, response: Response<VideoCallResponse>) {
                videoCallData.postValue(Resource.success(response.body()))
            }
            override fun onFailure(call: Call<VideoCallResponse>, t: Throwable) {
                videoCallData.postValue(Resource.error(t.toString(),null))
            }
        })
        return videoCallData
    }


    fun incomingCall(requestData: HashMap<String, String?>): MutableLiveData<Resource<ResponseBody>> {
        networkApi.incomingCall(requestData).enqueue(object : retrofit2.Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                incomingCall.postValue(Resource.success(response.body()))
            }
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                incomingCall.postValue(Resource.error(t.toString(),null))
            }
        })
        return incomingCall
    }


    fun getAddUserResponse(requestData: HashMap<String, String?>): MutableLiveData<Resource<ResponseBody>> {
        networkApi.getAddUsers(requestData).enqueue(object : retrofit2.Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                addUserData.postValue(Resource.success(response.body()))
            }
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                addUserData.postValue(Resource.error(t.toString(),null))
            }
        })
        return addUserData
    }

    fun getWalletsDetailsResponse(requestData: HashMap<String, String?>): MutableLiveData<Resource<WalletsResponse>> {
        networkApi.walletsdetails(requestData).enqueue(object : retrofit2.Callback<WalletsResponse> {
            override fun onResponse(call: Call<WalletsResponse>, response: Response<WalletsResponse>) {
                walletResponse.postValue(Resource.success(response.body()))
            }

            override fun onFailure(call: Call<WalletsResponse>, t: Throwable) {
                walletResponse.postValue(Resource.error(t.toString(), null))
            }
        })

        return walletResponse
    }


}