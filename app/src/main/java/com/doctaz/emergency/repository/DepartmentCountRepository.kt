package com.doctaz.emergency.repository

import androidx.lifecycle.MutableLiveData
import com.doctaz.emergency.model.DashboardMainPojo
import com.doctaz.emergency.retrofit.NetworkApi
import com.doctaz.emergency.utils.Resource

import retrofit2.Call
import retrofit2.Response
import retrofit2.http.FieldMap
import java.util.HashMap
import javax.inject.Inject

class DepartmentCountRepository @Inject constructor(private var networkApi: NetworkApi) {

    private var dashboardMainPojo = MutableLiveData<Resource<DashboardMainPojo>>()

    fun getDepartmentcountResponse( user_type: HashMap<String, String?>): MutableLiveData<Resource<DashboardMainPojo>> {
        networkApi.getDepartmentcount(user_type).enqueue(object : retrofit2.Callback<DashboardMainPojo> {
            override fun onResponse(call: Call<DashboardMainPojo>, response: Response<DashboardMainPojo>) {
                dashboardMainPojo.postValue(Resource.success(response.body()))
            }

            override fun onFailure(call: Call<DashboardMainPojo>, t: Throwable) {
                dashboardMainPojo.postValue(Resource.error(t.toString(), null))
            }
        })
        return dashboardMainPojo
    }
}