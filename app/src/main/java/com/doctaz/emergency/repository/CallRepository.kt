package com.doctaz.emergency.repository

import androidx.lifecycle.MutableLiveData
import com.doctaz.emergency.model.CallHistory
import com.doctaz.emergency.model.VideoCallResponse
import com.doctaz.emergency.retrofit.NetworkApi
import com.doctaz.emergency.utils.Resource

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import javax.inject.Inject

class CallRepository @Inject constructor(private var networkApi: NetworkApi) {
    private var videoCallData = MutableLiveData<Resource<VideoCallResponse>>()
    private var addUserData = MutableLiveData<Resource<ResponseBody>>()
    private var callHistory = MutableLiveData<Resource<CallHistory>>()

   /* fun getVideoCallResponse(requestData: HashMap<String, String?>): MutableLiveData<Resource<VideoCallResponse>> {
        networkApi.getVideoCall(requestData).enqueue(object : retrofit2.Callback<VideoCallResponse> {
            override fun onResponse(call: Call<VideoCallResponse>, response: Response<VideoCallResponse>) {
                videoCallData.postValue(Resource.success(response.body()))
            }
            override fun onFailure(call: Call<VideoCallResponse>, t: Throwable) {
                videoCallData.postValue(Resource.error(t.toString(),null))
            }
        })
        return videoCallData
    }

    fun getAddUserResponse(requestData: HashMap<String, String?>): MutableLiveData<Resource<ResponseBody>> {
        networkApi.getAddUsers(requestData).enqueue(object : retrofit2.Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                addUserData.postValue(Resource.success(response.body()))
            }
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                addUserData.postValue(Resource.error(t.toString(),null))
            }
        })
        return addUserData
    }*/

    fun getCallHistorySenderResponse( requestData: HashMap<String, String?>): MutableLiveData<Resource<CallHistory>> {
        networkApi.getcallhistorysenderData(requestData).enqueue(object : retrofit2.Callback<CallHistory> {
            override fun onResponse(call: Call<CallHistory>, response: Response<CallHistory>) {
                callHistory.postValue(Resource.success(response.body()))
            }

            override fun onFailure(call: Call<CallHistory>, t: Throwable) {
                callHistory.postValue(Resource.error(t.toString(), null))
            }
        })

        return callHistory
    }

    fun getCallHistoryReciverResponse( requestData: HashMap<String, String?>): MutableLiveData<Resource<CallHistory>> {
        networkApi.getcallhistoryReceiverData(requestData).enqueue(object : retrofit2.Callback<CallHistory> {
            override fun onResponse(call: Call<CallHistory>, response: Response<CallHistory>) {
                callHistory.postValue(Resource.success(response.body()))
            }

            override fun onFailure(call: Call<CallHistory>, t: Throwable) {
                callHistory.postValue(Resource.error(t.toString(), null))
            }
        })

        return callHistory
    }


}