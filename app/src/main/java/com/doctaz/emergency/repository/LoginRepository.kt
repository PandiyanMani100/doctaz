package com.doctaz.emergency.repository

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.doctaz.emergency.model.LoginResponse
import com.doctaz.emergency.model.RecentMessageResponse
import com.doctaz.emergency.retrofit.NetworkApi
import com.doctaz.emergency.room.ChatDatabase
import com.doctaz.emergency.room.ChatTable
import com.doctaz.emergency.utils.Resource

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Response
import javax.inject.Inject

class LoginRepository @Inject constructor(var networkApi: NetworkApi, var context: Application) {
    private var loginResponse = MutableLiveData<Resource<LoginResponse>>()
    private var recentChat = MutableLiveData<Resource<RecentMessageResponse>>()
    private lateinit var chatDatabase: ChatDatabase


    fun getLoginResponse(requestData: HashMap<String, String?>): LiveData<Resource<LoginResponse>> {
        networkApi.getLogin(requestData).enqueue(object : retrofit2.Callback<LoginResponse> {
            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                loginResponse.postValue(Resource.success(response.body()))
            }

            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                loginResponse.postValue(Resource.error(t.toString(), null))
            }
        })
        return loginResponse
    }

    fun getChatHistory(requestData: HashMap<String, String?>): MutableLiveData<Resource<RecentMessageResponse>> {
        networkApi.getChatHistory(requestData).enqueue(object : retrofit2.Callback<RecentMessageResponse> {
            override fun onResponse(call: Call<RecentMessageResponse>, response: Response<RecentMessageResponse>) {
                recentChat.postValue(Resource.success(response.body()))
            }

            override fun onFailure(call: Call<RecentMessageResponse>, t: Throwable) {
                recentChat.postValue(Resource.error(t.toString(), null))
            }
        })
        return recentChat
    }

    fun insertData(chat: ArrayList<ChatTable>) {
        chatDatabase = ChatDatabase.getChatDatabase(context)
        CoroutineScope(Dispatchers.IO).launch {
            chatDatabase.chatDao().addChat(chat)
        }
    }
}