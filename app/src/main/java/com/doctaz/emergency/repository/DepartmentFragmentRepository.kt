package com.doctaz.emergency.repository

import android.app.Application
import com.doctaz.emergency.room.ChatDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class DepartmentFragmentRepository @Inject constructor(var context: Application) {
    private lateinit var chatDatabase: ChatDatabase

    fun deleteChatTable() {
        chatDatabase = ChatDatabase.getChatDatabase(context)
        CoroutineScope(Dispatchers.IO).launch {
            chatDatabase.chatDao().deleteChatTable()
        }
    }
}