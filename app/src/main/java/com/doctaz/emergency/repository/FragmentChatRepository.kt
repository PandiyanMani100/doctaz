package com.doctaz.emergency.repository

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.doctaz.emergency.R
import com.doctaz.emergency.model.RecentMessageModel
import com.doctaz.emergency.retrofit.NetworkApi
import com.doctaz.emergency.room.ChatDatabase
import com.doctaz.emergency.room.ChatTable
import com.doctaz.emergency.view.activity.SessionManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Response
import javax.inject.Inject

class FragmentChatRepository @Inject constructor(var context: Application, var networkApi: NetworkApi, var sessionManager: SessionManager) {
    private var chatHistory = MutableLiveData<List<ChatTable>>()
    private var recentChatList = MutableLiveData<List<RecentMessageModel>>()
    private var recentChatArrayList = ArrayList<RecentMessageModel>()
    private var uniqueGroupMessageList = ArrayList<ChatTable>()
    private var uniqueIdList = ArrayList<String>()
    private lateinit var chatDatabase: ChatDatabase

    fun insertData(chat: ArrayList<ChatTable>) {
        chatDatabase = ChatDatabase.getChatDatabase(context)
        CoroutineScope(Dispatchers.IO).launch {
            chatDatabase.chatDao().addChat(chat)
        }
    }

    fun getUniqueGroup(): MutableLiveData<List<RecentMessageModel>> {
        var messageDesc = ""
        var messageDate = ""
        var unreadCount = 0
        var receiverName = ""
        var imageUri = ""
        var groupId = ""
        var receiverid = ""
        var recivertype = ""

        CoroutineScope(Dispatchers.IO).launch {
            chatDatabase = ChatDatabase.getChatDatabase(context)
            uniqueIdList.clear()
            uniqueGroupMessageList.clear()
            recentChatArrayList.clear()
            uniqueIdList.addAll(chatDatabase.chatDao().getUniqueGroups())
            for (item in 0 until uniqueIdList.size) {

                groupId = uniqueIdList[item]
                uniqueGroupMessageList.addAll(chatDatabase.chatDao().getLastReadChat(groupId))

                messageDesc = uniqueGroupMessageList[item].messageDesc
                messageDate = uniqueGroupMessageList[item].messageDate

                unreadCount = chatDatabase.chatDao().getUnReadMessageCount(groupId, context.resources.getString(R.string.unread))

                if (uniqueGroupMessageList[item].messageSender == sessionManager.getLoginId() && uniqueGroupMessageList[item].messageSenderType == sessionManager.getLoginType()) {

                    receiverid = uniqueGroupMessageList[item].messageReceiver
                    recivertype = uniqueGroupMessageList[item].messageReceiverType
                    receiverName = uniqueGroupMessageList[item].messageReceiverName
                    imageUri = uniqueGroupMessageList[item].messageReceiverImage
                } else {
                    receiverName = uniqueGroupMessageList[item].messageSenderName
                    imageUri = uniqueGroupMessageList[item].messageSenderImage
                    receiverid = uniqueGroupMessageList[item].messageSender
                    recivertype = uniqueGroupMessageList[item].messageSenderType
                }
                if (uniqueGroupMessageList[item].messageReceiver != sessionManager.getLoginId() && uniqueGroupMessageList[item].messageReceiverType != sessionManager.getLoginType()) {
                    receiverid = uniqueGroupMessageList[item].messageReceiver
                    recivertype = uniqueGroupMessageList[item].messageReceiverType
                    receiverName = uniqueGroupMessageList[item].messageReceiverName
                    imageUri = uniqueGroupMessageList[item].messageReceiverImage
                }
                recentChatArrayList.add(RecentMessageModel(receiverName, messageDesc, messageDate, unreadCount, imageUri, groupId, receiverid, recivertype))
            }
            recentChatList.postValue(recentChatArrayList)
        }

        return recentChatList
    }

    fun makeMessagesReadStatus(groupId: String) {
        chatDatabase = ChatDatabase.getChatDatabase(context)
        CoroutineScope(Dispatchers.IO).launch {
            chatDatabase.chatDao().makeChatReadStatus(groupId)
        }
    }

}