package com.doctaz.emergency.widgets

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet


class CustomTextView : androidx.appcompat.widget.AppCompatTextView {
    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {
        init()
    }

    fun init() {
     /*   val face = Typeface.createFromAsset(context.assets, "fonts/robos.ttf")
        this.typeface = face*/
    }

}