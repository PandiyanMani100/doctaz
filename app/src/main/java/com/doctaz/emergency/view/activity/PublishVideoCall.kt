package com.doctaz.emergency.view.activity

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.util.Patterns
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.doctaz.emergency.GpsUserPermission
import com.doctaz.emergency.Status
import com.doctaz.emergency.databinding.ActivityPublishVideoCallBinding
import com.doctaz.emergency.viewmodel.VideoCallViewModel

import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.opentok.android.*
import com.opentok.android.PublisherKit.PublisherListener
import com.opentok.android.Session.SessionListener
import dagger.hilt.android.AndroidEntryPoint
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.EasyPermissions.PermissionCallbacks
import java.util.*
import javax.inject.Inject
import com.doctaz.emergency.R


@AndroidEntryPoint
class PublishVideoCall : AppCompatActivity(), PermissionCallbacks, PublisherListener, SessionListener {

    private class SubscriberContainer(var container: ConstraintLayout, var toggleAudio: ImageView, var subscriber: Subscriber?)

    private lateinit var publishVideoCallBinding: ActivityPublishVideoCallBinding
    private lateinit var videoCallViewModel: VideoCallViewModel
    @Inject
    lateinit var userPermission: GpsUserPermission
    @Inject
    lateinit var sessionManager: SessionManager
    private var receiverId: String = ""
    private var sessionId: String = ""
    private var userCallType: String = ""
    private var call_minutes: String = ""
    private var call_amount: String = ""
    private var sessionToken = ""
    private var apiKey = ""
    private val subscriberLimit = 4
    private var mSession: Session? = null
    private var mPublisher: Publisher? = null
    private var mSubscribers: MutableList<SubscriberContainer>? = null
    private var sessionConnected = false
    private var muteVideoCall = true
    private var muteAudioCall = true
    private var subscriberAudioMute = true
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<ConstraintLayout>
    private lateinit var animation: Animation

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        publishVideoCallBinding = DataBindingUtil.setContentView(this, R.layout.activity_publish_video_call) //Binding initialization
        videoCallViewModel = ViewModelProvider(this).get(VideoCallViewModel::class.java) // ViewModel initialization
        bottomSheetBehavior = BottomSheetBehavior.from(publishVideoCallBinding.bottomSheetLayout) // Bottom sheet initialization
        init()
    }

    private fun init() {
        mSubscribers = ArrayList()
        sessionId = intent.getStringExtra("sessionId").toString()
        sessionToken = intent.getStringExtra("sessionToken").toString()
        receiverId = intent.getStringExtra("receiverId").toString()
        userCallType = intent.getStringExtra("callType").toString()
        apiKey = intent.getStringExtra("apiKey").toString()
        call_minutes=intent.getStringExtra("call_minutes").toString()
        call_amount=intent.getStringExtra("call_amount").toString()
        publishVideoCallBinding.userName.text = intent.getStringExtra("userName").toString()
        Glide.with(this).load(intent.getStringExtra("imageUrl").toString()).into((publishVideoCallBinding.profileImage))


        publishVideoCallBinding.cameraRotate.setOnClickListener(View.OnClickListener {
            if (mPublisher == null)
                return@OnClickListener
            mPublisher!!.cycleCamera()
        })

        publishVideoCallBinding.muteVideo.setOnClickListener(View.OnClickListener {
            if (mPublisher == null)
                return@OnClickListener
            if (muteVideoCall) {
                muteVideoCall = false
                publishVideoCallBinding.subscriber.visibility = View.VISIBLE
                publishVideoCallBinding.muteVideo.background = ContextCompat.getDrawable(this, R.drawable.ic_videocam)
                mPublisher!!.publishVideo = true
            } else {
                muteVideoCall = true
                publishVideoCallBinding.subscriber.visibility = View.GONE
                publishVideoCallBinding.muteVideo.background = ContextCompat.getDrawable(this, R.drawable.ic_videocam_off)
                mPublisher!!.publishVideo = false
            }

        })
        publishVideoCallBinding.muteAudio.setOnClickListener(View.OnClickListener {
            if (mPublisher == null)
                return@OnClickListener
            if (muteAudioCall) {
                muteAudioCall = false
                publishVideoCallBinding.muteAudio.background = ContextCompat.getDrawable(this, R.drawable.ic_microphone)
                mPublisher!!.publishAudio = true
            } else {
                muteAudioCall = true
                publishVideoCallBinding.muteAudio.background = ContextCompat.getDrawable(this, R.drawable.ic_mute_microphone)
                mPublisher!!.publishAudio = false
            }

        })

        publishVideoCallBinding.endCall.setOnClickListener {
            disconnectSession()
        }
        publishVideoCallBinding.addUser.setOnClickListener {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        publishVideoCallBinding.closeBottomSheet.setOnClickListener {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        }

        publishVideoCallBinding.inviteUser.setOnClickListener {
            if (publishVideoCallBinding.recipientEmailLabel.text.toString().trim().isEmpty())
                makeToast(resources.getString(R.string.email_empty_error_message))
            else if (!Patterns.EMAIL_ADDRESS.matcher(publishVideoCallBinding.recipientEmailLabel.text!!.toString().trim()).matches())
                makeToast(resources.getString(R.string.email_invalid_error_message))
            else if (publishVideoCallBinding.recipientMobileNumberLabel.text.toString().trim().isEmpty())
                makeToast(resources.getString(R.string.mobile_number_empty_error_message))
            else if (publishVideoCallBinding.messageLabel.text.toString().trim().isEmpty())
                makeToast(resources.getString(R.string.error_message_for_message))
            else if (sessionId.isEmpty())
                makeToast(resources.getString(R.string.room_number_empty_message))
            else {
                if (userPermission.isConnectedInternet())
                    addUser(publishVideoCallBinding.recipientEmailLabel.text.toString().trim(), publishVideoCallBinding.recipientMobileNumberLabel.text.toString().trim(), publishVideoCallBinding.messageLabel.text.toString().trim(), sessionId)
                else
                    makeToast(resources.getString(R.string.no_internet_connection))
            }
        }

        for (i in 0 until subscriberLimit) {
            val containerId = resources.getIdentifier("subscriberView$i", "id", this@PublishVideoCall.packageName)
            val toggleAudioId = resources.getIdentifier("toggleAudioSubscriber$i", "id", this@PublishVideoCall.packageName)
            mSubscribers!!.add(SubscriberContainer(findViewById(containerId), findViewById(toggleAudioId), null))
        }
        requestPermissions()
    }

    override fun onResume() {
        super.onResume()
        if (mSession == null) {
            return
        }
        mSession!!.onResume()
    }

    override fun onPause() {
        super.onPause()
        if (mSession == null) {
            return
        }
        mSession!!.onPause()
        if (isFinishing) {
            disconnectSession()
        }
    }

    override fun onDestroy() {
        disconnectSession()
        super.onDestroy()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onPermissionsGranted(requestCode: Int, perms: List<String>) {
        Log.d(TAG, "onPermissionsGranted:" + requestCode + ":" + perms.size)
    }

    override fun onPermissionsDenied(requestCode: Int, perms: List<String>) {
        Log.d(TAG, "onPermissionsDenied:" + requestCode + ":" + perms.size)
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            AppSettingsDialog.Builder(this)
                .setTitle(getString(R.string.title_settings_dialog))
                .setRationale(getString(R.string.rationale_ask_again))
                .setPositiveButton(getString(R.string.setting))
                .setNegativeButton(getString(R.string.cancel))
                .setRequestCode(settingsScreenPerms)
                .build()
                .show()
        }
    }

    @AfterPermissionGranted(videoAppPerm)
    private fun requestPermissions() {
        val perms = arrayOf(Manifest.permission.INTERNET, Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO)
        if (EasyPermissions.hasPermissions(this, *perms)) {
            if (userPermission.isConnectedInternet()) {
                if (userCallType.equals(resources.getString(R.string.firebase_call))) {
                    //makeToast("FireBase call")
                    initializeSession(apiKey, sessionId,sessionToken)
                } else
                    getToken()
            } else
                makeToast(resources.getString(R.string.no_internet_connection))
        } else
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_video_app), videoAppPerm, *perms)
    }

    override fun onConnected(session: Session) {
        sessionConnected = true
        mPublisher = Publisher.Builder(this@PublishVideoCall).name("publisher").build()
        mPublisher!!.setPublisherListener(this)
        mPublisher!!.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE, BaseVideoRenderer.STYLE_VIDEO_FILL)
        publishVideoCallBinding.subscriber.addView(mPublisher!!.view)
        mSession!!.publish(mPublisher)
    }

    override fun onDisconnected(session: Session) {
        sessionConnected = false
        mSession = null
    }

    override fun onError(session: Session, opentokError: OpentokError) {
        Toast.makeText(this, resources.getString(R.string.session_error), Toast.LENGTH_LONG).show()
        finish()
        overridePendingTransition(R.anim.from_left, R.anim.to_right)
    }

    private fun findFirstEmptyContainer(subscriber: Subscriber): SubscriberContainer? {
        for (c in mSubscribers!!) {
            if (c.subscriber == null) {
                return c
            }
        }
        return null
    }

    private fun findContainerForStream(stream: Stream): SubscriberContainer? {
        try {
            for (c in mSubscribers!!) {
                if (c.subscriber!!.stream.streamId == stream.streamId) {
                    return c
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }

    private fun addSubscriber(subscriber: Subscriber) {
        var subscriberCount = 0
        val container = findFirstEmptyContainer(subscriber)
        if (container == null) {
            Toast.makeText(this, resources.getString(R.string.maximum_subscriber_reached_message), Toast.LENGTH_LONG).show()
            return
        }
        container.subscriber = subscriber
        container.container.addView(subscriber.view)
        subscriber.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE, BaseVideoRenderer.STYLE_VIDEO_FILL)

        for (c in mSubscribers!!) {
            if (c.subscriber != null) {
                subscriberCount++
            }
        }

        if (subscriberCount == 1) {
            animation = AnimationUtils.loadAnimation(this@PublishVideoCall, R.anim.scale_down)
            publishVideoCallBinding.hideUserDetails.startAnimation(animation)
            Handler(Looper.getMainLooper()).postDelayed({
                publishVideoCallBinding.hideUserDetails.visibility = View.GONE
            }, 50)
            publishVideoCallBinding.cameraRotate.visibility = View.VISIBLE
            publishVideoCallBinding.muteVideo.visibility = View.VISIBLE
            publishVideoCallBinding.muteAudio.visibility = View.VISIBLE
            publishVideoCallBinding.addUser.visibility = View.VISIBLE
            publishVideoCallBinding.toggleAudioSubscriber0.visibility = View.VISIBLE
        } else if (subscriberCount == 2)
            publishVideoCallBinding.subscriberViewLayout1.visibility = View.VISIBLE
        else if (subscriberCount == 3)
            publishVideoCallBinding.subscriberViewLayout2.visibility = View.VISIBLE
        else if (subscriberCount == 4)
            publishVideoCallBinding.subscriberViewLayout3.visibility = View.VISIBLE


        container.toggleAudio.setOnClickListener {
            if (subscriberAudioMute) {
                subscriberAudioMute = false
                subscriber.subscribeToAudio = true
                container.toggleAudio.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_volume))
            } else {
                subscriberAudioMute = true
                subscriber.subscribeToAudio = false
                container.toggleAudio.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_volume_off))
            }
        }
        // container.toggleAudio.visibility = View.VISIBLE
    }

    private fun removeSubscriberWithStream(stream: Stream) {
        val container = findContainerForStream(stream) ?: return
        container.container.removeView(container.subscriber!!.view)
        container.toggleAudio.setOnClickListener(null)
        container.toggleAudio.visibility = View.INVISIBLE
        container.subscriber = null
    }

    override fun onStreamReceived(session: Session, stream: Stream) {
        Log.d(TAG, "onStreamReceived: New stream " + stream.streamId + " in session " + session.sessionId)
        val subscriber = Subscriber.Builder(this@PublishVideoCall, stream).build()
        mSession!!.subscribe(subscriber)
        addSubscriber(subscriber)
    }

    override fun onStreamDropped(session: Session, stream: Stream) {
        var afterStreamDroppedSubscriberCount = 0
        var disConnectedPosition = 0
        for (c in mSubscribers!!) {
            if (c.subscriber != null) {
                afterStreamDroppedSubscriberCount++
            }
        }
        if (afterStreamDroppedSubscriberCount - 1 == 0) {
            makeToast(resources.getString(R.string.call_diaconnected_receiver))
            finish()
            overridePendingTransition(R.anim.from_left, R.anim.to_right)
        } else {
            var findPosition = 0
            try {
                for (c in mSubscribers!!) {
                    findPosition++
                    if (c.subscriber!!.stream.streamId != null && c.subscriber!!.stream.streamId == stream.streamId)
                        disConnectedPosition = findPosition
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (disConnectedPosition == 1)
                publishVideoCallBinding.subscriberViewLayout0.visibility = View.GONE
            else if (disConnectedPosition == 2)
                publishVideoCallBinding.subscriberViewLayout1.visibility = View.GONE
            else if (disConnectedPosition == 3)
                publishVideoCallBinding.subscriberViewLayout2.visibility = View.GONE
            else if (disConnectedPosition == 4)
                publishVideoCallBinding.subscriberViewLayout3.visibility = View.GONE
            removeSubscriberWithStream(stream)
        }

    }

    override fun onStreamCreated(publisherKit: PublisherKit, stream: Stream) {
        Log.d(TAG, "onStreamCreated: Own stream " + stream.streamId + " created")
    }

    override fun onStreamDestroyed(publisherKit: PublisherKit, stream: Stream) {
        Log.d(TAG, "onStreamDestroyed: Own stream " + stream.streamId + " destroyed")
    }

    override fun onError(publisherKit: PublisherKit, opentokError: OpentokError) {
        makeToast(resources.getString(R.string.session_error))
        finish()
        overridePendingTransition(R.anim.from_left, R.anim.to_right)

    }

    private fun disconnectSession() {
        if (mSession == null || !sessionConnected) {
            finish()
            overridePendingTransition(R.anim.from_left, R.anim.to_right)
            return
        }
        sessionConnected = false
        if (mSubscribers!!.size > 0) {
            for (c in mSubscribers!!) {
                if (c.subscriber != null) {
                    mSession!!.unsubscribe(c.subscriber)
                    c.subscriber!!.destroy()
                }
            }
        }
        if (mPublisher != null) {
            publishVideoCallBinding.subscriber.removeView(mPublisher!!.view)
            mSession!!.unpublish(mPublisher)
            mPublisher!!.destroy()
            mPublisher = null
        }
        mSession!!.disconnect()
        if (!sessionId.equals("")) {
            unPublishCall(sessionId, "decline")
        }
        finish()
        overridePendingTransition(R.anim.from_left, R.anim.to_right)
    }

    private fun getToken() {
        videoCallViewModel.getVideoCallResponse(sessionManager.getLoginId(), receiverId,sessionManager.getLoginType(), "doctor",call_minutes,call_amount).observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    if (it.data!!.roomToken == "") {
                        Toast.makeText(this@PublishVideoCall, getString(R.string.roomnumbernbeeded), Toast.LENGTH_SHORT).show()
                        finish()
                        overridePendingTransition(R.anim.from_left, R.anim.to_right)
                    } else {
                        sessionId = it.data.roomNumber
                        initializeSession(it.data.apiKey, it.data.roomNumber, it.data.roomToken)
                    }
                }
                Status.ERROR -> Toast.makeText(this@PublishVideoCall, resources.getString(R.string.invalid), Toast.LENGTH_SHORT).show()
                else -> Toast.makeText(this@PublishVideoCall, resources.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show()
            }

        })
    }

    private fun addUser(email: String, mobile: String, message: String, roomNumber: String) {
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        videoCallViewModel.getAddUserResponse(email, mobile, message, roomNumber).observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> Toast.makeText(this, resources.getString(R.string.add_user_success_responce), Toast.LENGTH_SHORT).show()
                Status.ERROR -> Toast.makeText(this@PublishVideoCall, resources.getString(R.string.invalid), Toast.LENGTH_SHORT).show()
                else -> Toast.makeText(this@PublishVideoCall, resources.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun initializeSession(apiKey: String, sessionId: String, token: String) {
        mSession = Session.Builder(this, apiKey, sessionId).build()
        mSession!!.setSessionListener(this)
        mSession!!.connect(token)
    }

    private fun unPublishCall(roomNumber: String, callType: String) {
        val cbIntent = Intent()
        cbIntent.setClass(this, callhitapis::class.java)
        cbIntent.putExtra("room_number", roomNumber)
        cbIntent.putExtra("callType", callType)
        startService(cbIntent)
    }

    private fun makeToast(message: String) {
        Toast.makeText(this@PublishVideoCall, message, Toast.LENGTH_SHORT).show()
    }

    companion object {
        private val TAG = "simple-multiparty " + PublishVideoCall::class.java.simpleName
        private const val settingsScreenPerms = 123
        private const val videoAppPerm = 124
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.from_left, R.anim.to_right)
    }

}