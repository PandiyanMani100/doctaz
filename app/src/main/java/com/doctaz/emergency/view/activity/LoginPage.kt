package com.doctaz.emergency.view.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.doctaz.emergency.GpsUserPermission
import com.doctaz.emergency.R
import com.doctaz.emergency.Status
import com.doctaz.emergency.databinding.ActivityLoginPageBinding
import com.doctaz.emergency.databinding.ProgressDialogLayoutBinding
import com.doctaz.emergency.viewmodel.LoginPageViewmodel

import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class LoginPage : AppCompatActivity() {
    private lateinit var progressDialogLayoutBinding: ProgressDialogLayoutBinding
    private lateinit var loginPageBinding: ActivityLoginPageBinding
    private lateinit var loginPageViewmodel: LoginPageViewmodel
    private var dialog: AlertDialog? = null

    @Inject
    lateinit var userGpsUserPermission: GpsUserPermission

    @Inject
    lateinit var sessionManager: SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loginPageBinding = DataBindingUtil.setContentView(this, R.layout.activity_login_page)
        loginPageViewmodel = ViewModelProvider(this).get(LoginPageViewmodel::class.java)  //viewModel


        loginPageBinding.login.setOnClickListener {
            if (loginPageBinding.userMail.text!!.toString().trim().isEmpty())
                makeToast(resources.getString(R.string.email_empty_error_message))
            else if (!Patterns.EMAIL_ADDRESS.matcher(loginPageBinding.userMail.text!!.toString().trim()).matches())
                makeToast(resources.getString(R.string.email_invalid_error_message))
            else if (loginPageBinding.password.text!!.isEmpty())
                makeToast(resources.getString(R.string.password_empty_error_message))
            else {
                val inputMethodManager: InputMethodManager = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                val view: View = this.currentFocus!!
                inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
                if (userGpsUserPermission.isConnectedInternet()) {
                    showDialog()
                    login(loginPageBinding.userMail.text!!.toString().trim(), loginPageBinding.password.text.toString())
                } else makeToast(resources.getString(R.string.no_internet_connection))
            }

        }

        loginPageBinding.register.setOnClickListener { startActivity(Intent(this@LoginPage, RegisterNavigation::class.java)) }
        loginPageBinding.forgotPassword.setOnClickListener { makeToast(resources.getString(R.string.implemetation_proress)) }
    }

    private fun login(mailAddress: String, password: String) {
        loginPageViewmodel.getLoginResponse(mailAddress, password, sessionManager.getFirebaseTokenId().toString()).observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    if (it.data!!.loginType == resources.getString(R.string.patient)) {
                        sessionManager.setLoginDetails(it.data.loginType, it.data.name, it.data.patient.email, it.data.loginUserId, it.data.patient.imageUrl)
                        loginPageViewmodel.getRecentChatHistory(it.data.loginUserId, it.data.loginType).observe(this, Observer {
                            if (it.data!!.isNotEmpty())
                                loginPageViewmodel.insertData(it.data)
                            startActivity(Intent(this@LoginPage, Homepage::class.java).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK))
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        })
                    } else {
                        dialog!!.dismiss()
                        makeToast(resources.getString(R.string.invalid))
                    }
                }
                Status.ERROR -> showDialog()
                else -> makeToast(resources.getString(R.string.something_went_wrong))
            }
        })
    }

    private fun makeToast(message: String) {
        Toast.makeText(this@LoginPage, message, Toast.LENGTH_SHORT).show()
    }

    private fun showDialog() {
        if (dialog == null) {
            progressDialogLayoutBinding = ProgressDialogLayoutBinding.inflate(LayoutInflater.from(this))
            progressDialogLayoutBinding.messageTxt.visibility = View.GONE
            dialog = AlertDialog.Builder(this).setView(progressDialogLayoutBinding.root).show()
            dialog!!.setCancelable(false)
        } else if (dialog!!.isShowing)
            dialog!!.dismiss()
        else
            dialog!!.show()
    }
}

