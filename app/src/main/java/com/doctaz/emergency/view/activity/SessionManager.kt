package com.doctaz.emergency.view.activity

import android.content.Context
import android.content.SharedPreferences
import android.content.SharedPreferences.Editor
import dagger.hilt.android.qualifiers.ActivityContext
import javax.inject.Inject

class SessionManager @Inject constructor(@ActivityContext context: Context) {
    private var pref: SharedPreferences  // Shared Preferences
    private var editor: Editor  // Editor for Shared preferences
    private val PRIVATE_MODE = 0  // Shared pref mode
    private val PREF_NAME = "EmergencyAppInfo"  // Sharedpref name

    // All Shared Preferences Keys
    private var FirebaseTokenId: String = "FirebaseTokenId"
    private var CurrentAmount: String = "CurrentAmount"
    private var loginId = "loginId"
    private var loginType = "loginType"
    private var loginMail = "loginMail"
    private var loginName = "loginName"
    private var imageUrl = "imageUrl"
    private var fragmentCurrentPosition = "fragmentCurrentPosition"


    init {
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)!!
        editor = pref.edit()
    }

    fun setFirebaseTokenId(id: String) {
        editor.putString(FirebaseTokenId, id)
        editor.commit()
    }

    fun setWalletCurrentAmount(Current: String) {
        editor.putString(CurrentAmount, Current)
        editor.commit()
    }


    fun getFirebaseTokenId(): String? {
        return pref.getString(FirebaseTokenId, "")
    }

    fun setLoginDetails(login_Type: String, name: String, login_Mail: String, login_Id: String, image_url: String) {
        editor.putString(loginId, login_Id)
        editor.putString(loginName, name)
        editor.putString(loginMail, login_Mail)
        editor.putString(loginType, login_Type)
        editor.putString(imageUrl, image_url)
        editor.commit()
    }

    fun getLoginId(): String {
        return pref.getString(loginId, "").toString()
    }
    fun getWalletCurrentAmount(): String {
        return pref.getString(CurrentAmount, "").toString()
    }

    fun getLoginName(): String {
        return pref.getString(loginName, "").toString()
    }

    fun getLoginMail(): String {
        return pref.getString(loginMail, "").toString()
    }

    fun getLoginType(): String {
        return pref.getString(loginType, "").toString()
    }

    fun getLoginImageUrl(): String {
        return pref.getString(imageUrl, "").toString()
    }

    fun setFragmentCurrentPosition(position:Int){
        editor.putInt(fragmentCurrentPosition,position)
        editor.commit()
    }

    fun getFragmentCurrentPosition():Int{
        return pref.getInt(fragmentCurrentPosition, 0)
    }


    fun setClear() {
        editor.commit()
        editor.clear()
        editor.apply()
    }

}