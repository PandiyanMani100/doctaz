package com.doctaz.emergency.view.activity

import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.doctaz.emergency.R
import com.doctaz.emergency.databinding.ActivitySplashPageBinding

import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class SplashPage : AppCompatActivity() {
    @Inject
    lateinit var sessionManager: SessionManager
    private lateinit var splashPageBinding: ActivitySplashPageBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        splashPageBinding = DataBindingUtil.setContentView(this, R.layout.activity_splash_page)
        init()

    }
    fun init() {
        Handler(Looper.getMainLooper()).postDelayed({
            if(sessionManager.getLoginMail() == "")
            {
                startActivity(Intent(this@SplashPage, LoginPage::class.java).addFlags(FLAG_ACTIVITY_NEW_TASK or FLAG_ACTIVITY_CLEAR_TASK))

            }
            else
            {
                startActivity(Intent(this@SplashPage, Homepage::class.java).addFlags(FLAG_ACTIVITY_NEW_TASK or FLAG_ACTIVITY_CLEAR_TASK))
            }
           }, 1000)
    }

}



