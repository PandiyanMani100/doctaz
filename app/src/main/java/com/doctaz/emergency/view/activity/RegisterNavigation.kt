package com.doctaz.emergency.view.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.doctaz.emergency.R
import com.doctaz.emergency.databinding.ActivityRegisterNavigationBinding

import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RegisterNavigation : AppCompatActivity() {
    private lateinit var registerPageBinding: ActivityRegisterNavigationBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        registerPageBinding = DataBindingUtil.setContentView(this, R.layout.activity_register_navigation)

        registerPageBinding.backToLogin.setOnClickListener {
            onBackPressed()
            finish()
        }
        registerPageBinding.doctor.setOnClickListener { lunchRegisterPage(resources.getString(R.string.doctor))}
        registerPageBinding.nurse.setOnClickListener { lunchRegisterPage(resources.getString(R.string.nurse))}
        registerPageBinding.patient.setOnClickListener {lunchRegisterPage(resources.getString(R.string.patient))}
        registerPageBinding.healthCare.setOnClickListener { lunchRegisterPage(resources.getString(R.string.subscription_plan)) }
    }

    private fun lunchRegisterPage(type:String){
        startActivity(Intent(RegisterNavigation@this, RegisterPage::class.java).putExtra("loginType", type))
    }

}