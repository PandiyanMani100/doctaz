package com.doctaz.emergency.view.activity

import android.app.Activity
import android.app.Dialog
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.NonNull
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.doctaz.emergency.GpsUserPermission
import com.doctaz.emergency.R
import com.doctaz.emergency.adapter.ChatAdapter
import com.doctaz.emergency.constant.AppConstant
import com.doctaz.emergency.databinding.ActivityChatPageBinding
import com.doctaz.emergency.databinding.ProgressDialogLayoutBinding
import com.doctaz.emergency.room.ChatTable
import com.doctaz.emergency.viewmodel.ChatViewModel

import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList
import kotlin.math.ln



@AndroidEntryPoint
class ChatPage : AppCompatActivity() {

    private lateinit var progressDialogLayoutBinding: ProgressDialogLayoutBinding
    private lateinit var activityChatPageBinding: ActivityChatPageBinding
    private lateinit var updateChatRoomDatabase: ArrayList<ChatTable>
    private lateinit var chatArrayList: ArrayList<ChatTable>
    private lateinit var galleryLayoutAnimation: Animation
    private lateinit var chatViewModel: ChatViewModel
    private lateinit var chatAdapter: ChatAdapter
    private lateinit var messageUniqueId: String
    private lateinit var currentTime: String
    private lateinit var currentDate: String
    private var dialog: AlertDialog?=null
    private var galleryLayoutShow = true
    private var chatInsertStatus = true
    private var groupId = ""
    private var senderType = ""
    private var senderId = ""
    private var senderName = ""
    private var receiverId = ""
    private var receiverName = ""
    private var receiverType = ""
    private var receiverImageUrl = ""
    private var Tag = ""
    private var senderImageUrl = ""
    private var notificationId = 0



    @Inject
    lateinit var userPermission: GpsUserPermission

    @Inject
    lateinit var sessionManager: SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityChatPageBinding = DataBindingUtil.setContentView(this, R.layout.activity_chat_page)
        chatViewModel = ViewModelProvider(this).get(ChatViewModel::class.java)

        groupId = intent.getStringExtra("groupId").toString()
        receiverId = intent.getStringExtra("receiverId").toString()
        receiverName = intent.getStringExtra("userName").toString()
        receiverType = intent.getStringExtra("receiverType").toString()
        receiverImageUrl = intent.getStringExtra("imageUrl").toString()
        Tag = intent.getStringExtra("Tag").toString()
        notificationId = intent.getIntExtra("notificationId", 0)

        init()
    }

    private fun init() {
        chatArrayList = ArrayList()
        updateChatRoomDatabase = ArrayList()


        if (notificationId != 0)
            clearAllNotification()

        activityChatPageBinding.userName.text = receiverName
        if (intent.getStringExtra("lastSeen") != null && intent.getStringExtra("lastSeen").toString() != "")
            activityChatPageBinding.lastSeen.text = intent.getStringExtra("lastSeen").toString()
        Glide.with(this).load(receiverImageUrl).placeholder(ContextCompat.getDrawable(this,R.drawable.doctor_new)).into((activityChatPageBinding.profileImage))

        senderId = sessionManager.getLoginId()
        senderType = sessionManager.getLoginType()
        senderName = sessionManager.getLoginName()
        senderImageUrl = sessionManager.getLoginImageUrl()

        activityChatPageBinding.chatRecycler.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        chatAdapter = ChatAdapter(
            chatArrayList,
            sessionManager.getLoginType()
        )
        activityChatPageBinding.chatRecycler.adapter = chatAdapter
        activityChatPageBinding.chatRecycler.setHasFixedSize(true)


        activityChatPageBinding.iconBack.setOnClickListener {

           if (Tag.equals("Doctors_list")){
               var mov=Intent(this,DoctorsList::class.java)
               startActivity(mov)
               finish()
            } else{
               finish()

           }


        }
        activityChatPageBinding.camera.setOnClickListener {
            if (userPermission.cameraPermission() && userPermission.readExternalStoragePermission()) {
                startActivityResultForCamera.launch(Intent(MediaStore.ACTION_IMAGE_CAPTURE))

            } else
                userPermission.requestUserPermission()
        }

        activityChatPageBinding.message.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (s.toString().trim().isNotEmpty()) {
                    activityChatPageBinding.send.visibility = View.VISIBLE
                    activityChatPageBinding.voiceRecord.visibility = View.GONE
                    activityChatPageBinding.camera.visibility = View.GONE
                } else {
                    activityChatPageBinding.send.visibility = View.GONE
                    activityChatPageBinding.voiceRecord.visibility = View.VISIBLE
                    activityChatPageBinding.camera.visibility = View.VISIBLE
                }
            }
        })

        activityChatPageBinding.send.setOnClickListener { sendMessage("", "text") }
        activityChatPageBinding.voiceCall.setOnClickListener { makeToast(resources.getString(R.string.implemetation_proress)) }
        activityChatPageBinding.videoCall.setOnClickListener {


            alertShow(receiverId,receiverName,receiverImageUrl)


           // startActivity(Intent(this, PublishVideoCall::class.java).putExtra("receiverId", receiverId).putExtra("userName", receiverName).putExtra("imageUrl", receiverImageUrl))
        }

        activityChatPageBinding.attach.setOnClickListener {
            if (galleryLayoutShow) {
                galleryLayoutShow = false
                activityChatPageBinding.galleryLayout.visibility = View.VISIBLE
                galleryLayoutAnimation = AnimationUtils.loadAnimation(this@ChatPage, R.anim.scale_up)
                activityChatPageBinding.galleryLayout.startAnimation(galleryLayoutAnimation)
            } else {
                galleryLayoutShow = true
                galleryLayoutAnimation = AnimationUtils.loadAnimation(this@ChatPage, R.anim.scale_down)
                activityChatPageBinding.galleryLayout.startAnimation(galleryLayoutAnimation)
                Handler(Looper.getMainLooper()).postDelayed({
                    activityChatPageBinding.galleryLayout.visibility = View.GONE
                }, 50)
            }
        }

        activityChatPageBinding.pickImageGallery.setOnClickListener {
            closeGalleryPickLayout()
            if (userPermission.cameraPermission() && userPermission.readExternalStoragePermission())
                startActivityResultForGallery.launch(Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI).setType("image/* video/*"))
            else
                userPermission.requestUserPermission()
        }
        activityChatPageBinding.pickImageCamera.setOnClickListener {
            closeGalleryPickLayout()
            if (userPermission.cameraPermission() && userPermission.readExternalStoragePermission())
                startActivityResultForCamera.launch(Intent(MediaStore.ACTION_IMAGE_CAPTURE))
            else
                userPermission.requestUserPermission()
        }

        activityChatPageBinding.document.setOnClickListener {
            closeGalleryPickLayout()
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            val uri = Uri.parse(
                (Environment.getExternalStorageDirectory().path
                        + File.separator) + "myFolder" + File.separator
            )
            intent.setDataAndType(uri, "text/csv")
            startActivity(Intent.createChooser(intent, "Open folder"))
        }

        getChatHistory()
    }


    private val startActivityResultForGallery = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if (it.resultCode == Activity.RESULT_OK)
            imageMessage(it.data!!.data!!)
    }

    private val startActivityResultForCamera = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if (it.resultCode == Activity.RESULT_OK)
            imageMessage(it.data!!.data!!)
    }

    private fun imageMessage(imageUri: Uri) {
        if ("content" == imageUri.scheme) {
            val cursor: Cursor? = this.contentResolver.query(imageUri, arrayOf(MediaStore.Images.ImageColumns.DATA), null, null, null)
            cursor!!.moveToFirst()
            val filePath = cursor.getString(0)
            cursor.close()
            sendMessage(filePath, "picture")
        }
    }

    fun alertShow(receiverId:String,receiverName:String,imageUrl:String){
        val dialog = Dialog(this,R.style.theme_dialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.alert_videocall_layout)
        val body = dialog.findViewById(R.id.tvBody) as TextView
        val cancel = dialog.findViewById(R.id.cancel) as TextView
        val iv_cancel = dialog.findViewById(R.id.iv_cancel) as ImageView
        val conformation = dialog.findViewById(R.id.conformation) as TextView
        val tv_addamount = dialog.findViewById(R.id.tv_addamount) as TextView
        val ed_duration = dialog.findViewById(R.id.ed_duration) as EditText
        var duration=ed_duration.text.toString()
        var time =""
        var product:String?=""
        tv_addamount.text="$ "+sessionManager.getWalletCurrentAmount()

        ed_duration.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                time=s.toString()

                if ( time.equals("")){
                    time="0.0"
                    product = ""+2 * time.toFloat()
                    conformation.text="Proceed To Video Call $ "+product.toString()
                }else{
                    product = ""+2 * time.toFloat()
                    conformation.text="Proceed To Video Call $ "+product.toString()
                }
                /* if (start == 12) {
                     Toast.makeText(applicationContext, "Maximum Limit Reached", Toast.LENGTH_SHORT)
                         .show()
                 }*/
            }

        })

        conformation.text="Proceed To Video Call"



        conformation.setOnClickListener {




            if(product!!.equals("")){
                makeToast("Please set the time")

            }else{
                if ( product!!.toFloat() <= sessionManager.getWalletCurrentAmount().toFloat()){
                    startActivity(Intent(this, PublishVideoCall::class.java).putExtra("receiverId", receiverId).putExtra("userName", receiverName).putExtra("receiverType", "doctor").putExtra("imageUrl", imageUrl).putExtra("call_minutes", time).putExtra("call_amount", ""+product!!.toFloat()))
                    dialog.dismiss()
                }else{
                    makeToast("Wallet Amount too less make the call")

                }





            }






        }
        cancel.setOnClickListener { dialog.dismiss() }
        dialog.show()
        iv_cancel.setOnClickListener { dialog.dismiss() }
        dialog.show()

    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (Tag.equals("Doctors_list")){
            var mov=Intent(this,DoctorsList::class.java)
            startActivity(mov)
            finish()
        } else{
            finish()

        }


    }




    private fun sendMessage(imageUri: String, messageType: String) {
        if (userPermission.isConnectedInternet()) {
            if (groupId.isNotEmpty() && receiverId.isNotEmpty() /*&& receiverImageUrl.isNotEmpty()*/ && senderId.isNotEmpty() ) {
                val message = activityChatPageBinding.message.text.toString().trim().replace("(?m)^[ \t]*\r?\n".toRegex(), "")
                messageUniqueId = SimpleDateFormat("EEE,dd-MM-yyyy HH:mm:ss").format(Calendar.getInstance().time)
                currentTime = SimpleDateFormat("EEE h:mm a").format(Calendar.getInstance().time)
                currentDate = SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().time)
                updateChatRoomDatabase.add(ChatTable(0, groupId, currentTime, message, imageUri, receiverId, receiverImageUrl, receiverName, receiverType, senderId, senderImageUrl, senderName, senderType, resources.getString(R.string.read)))
                chatArrayList.addAll(updateChatRoomDatabase)
                chatAdapter.updateList(chatArrayList)
                activityChatPageBinding.chatRecycler.smoothScrollToPosition(chatArrayList.size)
                chatViewModel.insertData(updateChatRoomDatabase)
                activityChatPageBinding.message.text = null
                updateChatRoomDatabase.clear()
                if (imageUri == "")
                    chatViewModel.sendMessage(senderId, senderType, groupId, message, imageUri)

                val volume = (1 - ln(100 - 20.toDouble()) / ln(100.toDouble())).toFloat()
                val mediaPlayer = MediaPlayer.create(this, R.raw.message_send_tone)
                val audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 20, 0)
                mediaPlayer.setVolume(volume, volume)
                mediaPlayer.start()
            } else
                makeToast(resources.getString(R.string.something_went_wrong))
        } else
            Snackbar.make(activityChatPageBinding.parent, resources.getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT).show()
    }

    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == AppConstant.chatPageRequestCode) {
            // startActivityResult.launch(Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI).setType("image/* video/*"))
        } else
            userPermission.requestUserPermission()
    }

    private fun closeGalleryPickLayout() {
        galleryLayoutShow = true
        galleryLayoutAnimation = AnimationUtils.loadAnimation(this@ChatPage, R.anim.scale_down)
        activityChatPageBinding.galleryLayout.startAnimation(galleryLayoutAnimation)
        Handler(Looper.getMainLooper()).postDelayed({
            activityChatPageBinding.galleryLayout.visibility = View.GONE
        }, 50)
    }

    private fun makeToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun onResume() {
        super.onResume()
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: ArrayList<ChatTable>) {
        chatArrayList.addAll(event)
        chatAdapter.updateList(chatArrayList)
        activityChatPageBinding.chatRecycler.smoothScrollToPosition(chatArrayList.size)
        //chatViewModel.insertData(event)
        chatViewModel.makeChatReadStatus(groupId)
    }

    private fun getChatHistory() {
        if (dialog==null || !dialog!!.isShowing)
            showProgressBar()
        if (groupId.isNotEmpty()) {
            chatViewModel.getChatHistory(groupId).observe(this, Observer {
                if (it != null && it.isEmpty() && sessionManager.getLoginId().isNotEmpty() && chatInsertStatus) {
                    if (userPermission.isConnectedInternet()) {
                        chatViewModel.getRecentChatHistory(sessionManager.getLoginId(), sessionManager.getLoginType()).observe(this, Observer {
                            if (it != null) {
                                chatViewModel.insertData(it.data!!)
                                chatInsertStatus=false
                                Handler(Looper.getMainLooper()).postDelayed({
                                    getChatHistory()
                                }, 300)
                            }
                        })
                    } else
                        makeToast(resources.getString(R.string.no_internet_connection))
                } else if (it != null) {
                    chatArrayList.addAll(it)
                    chatAdapter.updateList(chatArrayList)
                    activityChatPageBinding.chatRecycler.smoothScrollToPosition(chatArrayList.size)
                    if (dialog!!.isShowing)
                        dialog!!.dismiss()
                }

            })
        } else makeToast(resources.getString(R.string.require_details_not_found))

    }

    private fun clearAllNotification() {
        val notificationManager: NotificationManager = this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancel(notificationId)
    }

    private fun showProgressBar() {
        progressDialogLayoutBinding = ProgressDialogLayoutBinding.inflate(LayoutInflater.from(this))
        dialog = AlertDialog.Builder(this).setView(progressDialogLayoutBinding.root).show()
        dialog!!.setCancelable(false)
    }
}


