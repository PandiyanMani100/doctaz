package com.doctaz.emergency.view.fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.doctaz.emergency.R
import com.doctaz.emergency.adapter.CallHistoryAdapter
import com.doctaz.emergency.adapter.DepartListOnItemClickListener
import com.doctaz.emergency.databinding.FragmentCallBinding
import com.doctaz.emergency.model.CallHistory
import com.doctaz.emergency.model.CallHistoryItem
import com.doctaz.emergency.model.CallHistoryResponse
import com.doctaz.emergency.retrofit.RetrofitInstance
import com.doctaz.emergency.view.activity.SessionManager
import com.doctaz.emergency.viewmodel.CallViewModel
import com.doctaz.emergency.viewmodel.DepartmentFragmentViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_call.*
import kotlinx.android.synthetic.main.fragment_call.view.*
import org.greenrobot.eventbus.EventBus
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@AndroidEntryPoint
class CallFragment : Fragment() {
    private lateinit var fragmentCallBinding: FragmentCallBinding
    private  lateinit var callHistoryAdapter: CallHistoryAdapter
    private  var departmentFragmentViewModel: DepartmentFragmentViewModel?=null
    private  var callViewModel: CallViewModel?=null
    private  var callHistoryResponse: CallHistoryResponse?=null
    private var callHistory:ArrayList<CallHistoryItem>?=null
    private var callResponse: CallHistory?=null
    private var departListOnItemClickListener: DepartListOnItemClickListener? = null
    var user_id:String?=""

    @Inject
    lateinit var sessionManager: SessionManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentCallBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_call, container, false)
        this.sessionManager
        callViewModel = ViewModelProvider(this).get(CallViewModel::class.java)
        callHistory = ArrayList<CallHistoryItem>()
        callHistory!!.clear()


        getCallHistorySenderList()

        user_id=sessionManager.getLoginId()

        return fragmentCallBinding.root
    }
    override fun onResume() {
        super.onResume()
      getCallHistoryReciverList()
       getCallHistorySenderList()


       /* if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this)*/
    }
    private fun getCallHistoryReciverList() {

        val requestData=HashMap<String,String?>()
        requestData["sender_id"]=sessionManager.getLoginId()
        requestData["sender_type"]=sessionManager.getLoginType()


        RetrofitInstance.getRetrofitInstance().getcallhistorysenderData(requestData).enqueue(object : Callback<CallHistory?> {
            override fun onResponse(call: Call<CallHistory?>?, response: Response<CallHistory?>) {
                if (response.body() != null) {
                    Log.e("SignUpEmailMobileResp", response.toString())

                    if (response.body()!=null) {

                        for (i in 0 until  response!!.body()!!.size){
                            callHistory!!.add(response!!.body()!!!![i])

                        }
                        if (callHistory!!.size.equals(0)){
                            nocallTxt.visibility = View.VISIBLE
                            }else{
                            if (nocallTxt.visibility == View.VISIBLE)
                                nocallTxt.visibility = View.GONE
                        }


                        callList.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
                        callHistoryAdapter = CallHistoryAdapter(
                                requireActivity(),
                                callHistory!!,
                                user_id!!
                            )
                        callList.adapter = callHistoryAdapter
                        callList.visibility = View.VISIBLE

                        EventBus.getDefault().post(response.body())
                    }else nocallTxt.visibility = View.VISIBLE

                }
                else nocallTxt.visibility = View.VISIBLE

            }

            override fun onFailure(call: Call<CallHistory?>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })





/*
        callViewModel!!.getCallHistorySenderResponse(sessionManager.getLoginType(),sessionManager.getLoginId()).observe(requireActivity(), Observer {
            when (it.status) {
                Status.SUCCESS -> {

                  shimmerViewContainer.visibility = View.GONE
                  shimmerViewContainer.stopShimmerAnimation()
                    callResponse=it.data!!
                 */
/*for (i in 0 until it.data!!.size){
                     callHistory!!.add(it!!.data!![i])
                 }*//*


                    if( callResponse!=null){
                        for (i in 0 until callResponse!!.size){
                            callHistory!!.add(callResponse!![i])
                        }

                        callList.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
                        callHistoryAdapter = CallHistoryAdapter(requireActivity(),callHistory!!)
                        callList.adapter = callHistoryAdapter
                        callList.visibility = View.VISIBLE
                    }



//                    departmentFragment!!.shimmerViewContainer.stopShimmerAnimation()
                }
                Status.LOADING -> Toast.makeText(activity, it.data.toString(), Toast.LENGTH_SHORT).show()
                Status.ERROR -> Toast.makeText(activity, resources.getString(R.string.invalid), Toast.LENGTH_SHORT).show()
                Status.NOINTERNET -> Toast.makeText(activity, resources.getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show()
                else -> Toast.makeText(activity, resources.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show()
            }
        })
*/
    }

    private fun getCallHistorySenderList() {


         val requestData=HashMap<String,String?>()
   requestData["receiver_id"]=sessionManager.getLoginId()
   requestData["receiver_type"]=sessionManager.getLoginType()


         RetrofitInstance.getRetrofitInstance().getcallhistorysenderData(requestData).enqueue(object : Callback<CallHistory?> {
             override fun onResponse(call: Call<CallHistory?>?, response: Response<CallHistory?>) {
                 if (response.body() != null) {
                     Log.e("SignUpEmailMobileResp", response.toString())

                     if (response.body() != null) {

                             for (i in 0 until  response!!.body()!!.size){
                                 callHistory!!.add(response!!.body()!!!![i])
                     }
                         getCallHistoryReciverList()

                     EventBus.getDefault().post(response.body())
                 }

             }

                     }

             override fun onFailure(call: Call<CallHistory?>, t: Throwable) {
                 TODO("Not yet implemented")
             }
         })
    }



/*
        callViewModel!!.getCallHistorySenderResponse(sessionManager.getLoginType(),sessionManager.getLoginId()).observe(requireActivity(), Observer {
            when (it.status) {
                Status.SUCCESS -> {
//                    callHistory = it.data!!
                    shimmerViewContainer.visibility = View.GONE
                    shimmerViewContainer.stopShimmerAnimation()


                    callResponse=it.data

                  */
/*  for (i in 0 until it.data!!.size){
                        callHistory!!.add(it.data[i])
                    }*//*

                    getCallHistoryReciverList()

                }
                Status.LOADING -> Toast.makeText(activity, it.data.toString(), Toast.LENGTH_SHORT).show()
                Status.ERROR -> Toast.makeText(activity, resources.getString(R.string.invalid), Toast.LENGTH_SHORT).show()
                Status.NOINTERNET -> Toast.makeText(activity, resources.getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show()
                else -> Toast.makeText(activity, resources.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show()
            }
        })
*/
         }


