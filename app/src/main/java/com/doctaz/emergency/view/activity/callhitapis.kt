package com.doctaz.emergency.view.activity

import android.app.IntentService
import android.content.Intent
import android.util.Log
import com.doctaz.emergency.constant.Iconstant
import com.mindorks.kotnetworking.KotNetworking
import com.mindorks.kotnetworking.common.Priority
import java.util.*


class callhitapis : IntentService("callhitapis IntentService") {
    override fun onHandleIntent(intent: Intent?) {

        var urltohit:String = ""
        val room_number = intent!!.getStringExtra("room_number")
        val typeofcall = intent.getStringExtra("callType")

        if(typeofcall.equals("decline"))
        {
            urltohit= Iconstant.baseUrl+"call/decline_call"
        }
        else if(typeofcall.equals("accept"))
        {
            urltohit=Iconstant.baseUrl+"call/accept_call"
        }
        var header = HashMap<String, String>()
        header.put("room_number", room_number!!)

        KotNetworking.post(urltohit)
            .addBodyParameter(header)
            .setTag(this)
            .setPriority(Priority.HIGH)
            .build()
            .getAsJSONObject { response, error ->
                if (error != null) {
                    Log.d("api res--", error.toString())
                } else {
                    Log.d("api res--", response.toString())
                }
            }
    }
}