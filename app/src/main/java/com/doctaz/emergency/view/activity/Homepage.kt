package com.doctaz.emergency.view.activity

import android.content.Intent
import android.graphics.drawable.GradientDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.widget.LinearLayout
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.doctaz.emergency.R
import com.doctaz.emergency.databinding.ActivityHomePageBinding
import com.doctaz.emergency.service.ForegroundService
import com.doctaz.emergency.view.fragment.CallFragment
import com.doctaz.emergency.view.fragment.ChatFragment
import com.doctaz.emergency.view.fragment.DepartmentFragment
import com.doctaz.emergency.view.fragment.ProfileFragment

import com.google.android.material.tabs.TabLayout
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class Homepage : AppCompatActivity() {

    var OVERLAY_PERMISSION_WAZE_REQ_CODE = 555
    private lateinit var homePageBinding: ActivityHomePageBinding
    private lateinit var tabArrayList: ArrayList<String>
    private lateinit var fragmentList: ArrayList<Fragment>
    private lateinit var viewPagerAdapter: ViewPagerAdapter
    private val timeInterval = 2000
    private var backPressed: Long = 0


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        homePageBinding = DataBindingUtil.setContentView(this, R.layout.activity_home_page)
        init()
        startService()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun init() {
        tabArrayList = ArrayList()
        fragmentList = ArrayList()
        viewPagerAdapter = ViewPagerAdapter(supportFragmentManager)
        tabArrayList.add(resources.getString(R.string.depts))
        tabArrayList.add(resources.getString(R.string.chat))
        tabArrayList.add(resources.getString(R.string.calls))
        tabArrayList.add(resources.getString(R.string.profile))
        val linearLayout: LinearLayout = homePageBinding.navigationTabs.getChildAt(0) as LinearLayout
        linearLayout.showDividers = LinearLayout.SHOW_DIVIDER_MIDDLE
        val drawable = GradientDrawable()
        drawable.setColor(ContextCompat.getColor(this, R.color.white))
        drawable.setSize(1, 1)
        linearLayout.dividerPadding = 10
        linearLayout.dividerDrawable = drawable
        for (i in tabArrayList.indices)
            homePageBinding.navigationTabs.addTab(homePageBinding.navigationTabs.newTab().setText(tabArrayList[i]))
        viewPagerAdapter.addFragment(DepartmentFragment())
        viewPagerAdapter.addFragment(ChatFragment())
        viewPagerAdapter.addFragment(CallFragment())
        viewPagerAdapter.addFragment(ProfileFragment())
        homePageBinding.navigationTabs.setupWithViewPager(homePageBinding.viewPager)
        homePageBinding.viewPager.adapter = viewPagerAdapter
        homePageBinding.viewPager.offscreenPageLimit = 3
        if (!Settings.canDrawOverlays(this)) {
            val alertDialog: AlertDialog.Builder = AlertDialog.Builder(this@Homepage)
            alertDialog.setTitle(getString(R.string.appnamess))
            alertDialog.setMessage(getString(R.string.enableper))
            alertDialog.setPositiveButton(
                getString(R.string.enables)
            ) { _, _ ->
                askForSystemOverlayPermission()
            }
            alertDialog.setNegativeButton(
                getString(R.string.closee)
            ) { _, _ -> }
            val alert: AlertDialog = alertDialog.create()
            alert.setCanceledOnTouchOutside(true)
            alert.show()

        }

        homePageBinding.navigationTabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {}
            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }


    override fun onDestroy() {
        super.onDestroy()
        stopService()
    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) : FragmentStatePagerAdapter(manager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

        override fun getItem(position: Int): Fragment {
            return fragmentList[position]
        }

        override fun getCount(): Int {
            return tabArrayList.size
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return tabArrayList[position]
        }

        fun addFragment(fragment: Fragment) {
            fragmentList.add(fragment)
        }
    }

    private fun askForSystemOverlayPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(applicationContext)) {

            val intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:$packageName"))
            startActivityForResult(intent, OVERLAY_PERMISSION_WAZE_REQ_CODE)
        }
    }

    override fun onBackPressed() {
        if (backPressed + timeInterval > System.currentTimeMillis()) {
            super.onBackPressed()
            return
        } else
            Toast.makeText(baseContext, resources.getString(R.string.press_exit), Toast.LENGTH_SHORT).show()
        backPressed = System.currentTimeMillis()
    }


    fun startService() {
        val serviceIntent = Intent(this, ForegroundService::class.java)
        serviceIntent.putExtra("inputExtra", "Foreground Service Example in Android")
        ContextCompat.startForegroundService(this, serviceIntent)
    }


    fun stopService() {
        val serviceIntent = Intent(this, ForegroundService::class.java)
        stopService(serviceIntent)
    }
}




