package com.doctaz.emergency.view.activity

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.doctaz.emergency.GpsUserPermission
import com.doctaz.emergency.R
import com.doctaz.emergency.Status
import com.doctaz.emergency.databinding.ActivityRegisterPageBinding
import com.doctaz.emergency.databinding.GalleryPickerLayoutBinding
import com.doctaz.emergency.databinding.ProgressDialogLayoutBinding
import com.doctaz.emergency.viewmodel.RegisterPageViewModel
import com.mynameismidori.currencypicker.CurrencyPicker
import com.ybs.countrypicker.CountryPicker
import com.ybs.countrypicker.CountryPickerListener
import dagger.hilt.android.AndroidEntryPoint
import java.io.*
import java.util.*
import javax.inject.Inject


@AndroidEntryPoint
class RegisterPage : AppCompatActivity() {
    private lateinit var galleryPickerLayoutBinding: GalleryPickerLayoutBinding
    private lateinit var progressDialogLayoutBinding: ProgressDialogLayoutBinding
    private lateinit var activityRegisterPageBinding: ActivityRegisterPageBinding
    private lateinit var registerPageViewModel: RegisterPageViewModel
    private var galleryPickerDialog: AlertDialog? = null
    private var dialog: AlertDialog? = null
    private var gender = ""
    private var bloodGroup = ""
    private var imageType = ""
    var picker: CountryPicker? = null
    private var imageChooseType = false
    private var department = ""
    val genderList = arrayOf("Select Sex", "Male", "Female")
    private val bloodGroupList = arrayOf(
        "Select Blood Group",
        "A+",
        "A-",
        "B+",
        "B-",
        "AB+",
        "AB-",
        "O+",
        "O-"
    )
    private val imageTypeList = arrayOf("ProfileImage", "MedicalDocument", "idcaard")

    @Inject
    lateinit var gpsUserPermission: GpsUserPermission


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityRegisterPageBinding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_register_page
        )
        registerPageViewModel = ViewModelProvider(this).get(RegisterPageViewModel::class.java)

        activityRegisterPageBinding.iconBack.setOnClickListener { onBackPressed() }

        val loginType = intent.getStringExtra("loginType")
        if (loginType != null && loginType == resources.getString(R.string.doctor)) {
            activityRegisterPageBinding.patientSpecialField.visibility = View.GONE
            activityRegisterPageBinding.doctorSpacialFieldLayout.visibility = View.VISIBLE
            activityRegisterPageBinding.medicalDocumentLayout.visibility = View.VISIBLE
            activityRegisterPageBinding.registrationCertificateLayout.visibility = View.VISIBLE
            activityRegisterPageBinding.subscriptionPlanLayout.visibility = View.GONE

            activityRegisterPageBinding.save.text = getString(R.string.register_as_doctor)

            val doctortype = resources.getStringArray(R.array.doctortype)
            val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, doctortype)
            activityRegisterPageBinding.doctortypespinnear.adapter = adapter
            picker = CountryPicker.newInstance(getString(R.string.selectcountry))
            activityRegisterPageBinding.countryspinnear.setOnClickListener {
                picker!!.show(getSupportFragmentManager(), "COUNTRY_PICKER");
            }
            picker!!.setListener(CountryPickerListener { name: String?, code: String?, dialCode: String?, flagDrawableResID: Int ->
                activityRegisterPageBinding.countryspinnear.setText(name)
                picker!!.dismiss()
            })


            val picker = CurrencyPicker.newInstance(getString(R.string.slecu)) // dialog title

            picker.setListener { name, code, symbol, flagDrawableResID ->
                activityRegisterPageBinding.currencyspinnear.setText(name)
                picker!!.dismiss()
            }
            activityRegisterPageBinding.currencyspinnear.setOnClickListener {
                picker.show(supportFragmentManager, "CURRENCY_PICKER")
            }

            activityRegisterPageBinding.chooseidcard.setOnClickListener {
                showGalleryDialog()
                imageType = imageTypeList[2]
            }




        } else if (loginType != null && loginType == resources.getString(R.string.nurse)) {

            val doctortype = resources.getStringArray(R.array.nursetype)
            val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, doctortype)
            activityRegisterPageBinding.doctortypespinnear.adapter = adapter

            activityRegisterPageBinding.doctortype.setText(R.string.nursetype)
            activityRegisterPageBinding.save.text = getString(R.string.register_as_nurse)
            activityRegisterPageBinding.patientSpecialField.visibility = View.GONE
            activityRegisterPageBinding.doctorSpacialFieldLayout.visibility = View.GONE
            activityRegisterPageBinding.medicalDocumentLayout.visibility = View.VISIBLE
            activityRegisterPageBinding.registrationCertificateLayout.visibility = View.VISIBLE
            activityRegisterPageBinding.subscriptionPlanLayout.visibility = View.GONE

        } else if (loginType != null && loginType == resources.getString(R.string.patient)) {
            activityRegisterPageBinding.doctortype.visibility = View.GONE
            activityRegisterPageBinding.doctortypespinnear.visibility = View.GONE
            activityRegisterPageBinding.save.text = getString(R.string.register_as_patient)
            activityRegisterPageBinding.patientSpecialField.visibility = View.VISIBLE
            activityRegisterPageBinding.doctorSpacialFieldLayout.visibility = View.GONE
            activityRegisterPageBinding.medicalDocumentLayout.visibility = View.GONE
            activityRegisterPageBinding.registrationCertificateLayout.visibility = View.GONE
            activityRegisterPageBinding.subscriptionPlanLayout.visibility = View.GONE
        } else if (loginType != null && loginType == resources.getString(R.string.subscription_plan)) {

            activityRegisterPageBinding.save.text = getString(R.string.register_as_hf)
            activityRegisterPageBinding.doctortype.visibility = View.GONE
            activityRegisterPageBinding.doctortypespinnear.visibility = View.GONE

            activityRegisterPageBinding.country.visibility = View.GONE
            activityRegisterPageBinding.countryspinnear.visibility = View.GONE
            activityRegisterPageBinding.currencytext.visibility = View.GONE
            activityRegisterPageBinding.currencyspinnear.visibility = View.GONE


            activityRegisterPageBinding.patientSpecialField.visibility = View.GONE
            activityRegisterPageBinding.doctorSpacialFieldLayout.visibility = View.GONE
            activityRegisterPageBinding.medicalDocumentLayout.visibility = View.GONE
            activityRegisterPageBinding.registrationCertificateLayout.visibility = View.GONE
            activityRegisterPageBinding.subscriptionPlanLayout.visibility = View.VISIBLE
            activityRegisterPageBinding.profileImage.visibility = View.GONE
            activityRegisterPageBinding.title.text = resources.getString(R.string.register_as_health_care)
            activityRegisterPageBinding.nameTxt.text = resources.getString(R.string.facility_name)
            activityRegisterPageBinding.emailTxt.text = resources.getString(R.string.facility_email)


        }


        val genderAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, genderList)
        genderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        activityRegisterPageBinding.spinnerGender.adapter = genderAdapter

        val bloodGroupAdapter = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_item,
            bloodGroupList
        )
        bloodGroupAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        activityRegisterPageBinding.bloodGroup.adapter = bloodGroupAdapter

        activityRegisterPageBinding.spinnerGender.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0)
                    gender = genderList[position]
            }
        }

        activityRegisterPageBinding.bloodGroup.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0)
                    bloodGroup = bloodGroupList[position]
            }
        }

        activityRegisterPageBinding.calenderIcon.setOnClickListener {
            val date = StringBuffer()
            val c = Calendar.getInstance()
            val mYear = c[Calendar.YEAR]
            val mMonth = c[Calendar.MONTH]
            val mDay = c[Calendar.DAY_OF_MONTH]

            val datePickerDialog = DatePickerDialog(this, { v, year, monthOfYear, dayOfMonth ->
                if (dayOfMonth.toString().length == 1)
                    date.append("0").append(dayOfMonth.toString()).append("/")
                else
                    date.append(dayOfMonth.toString()).append("/")
                if (monthOfYear.toString().length == 1)
                    date.append("0").append(monthOfYear + 1).append("/").append(year)
                else
                    date.append(monthOfYear + 1).append("/").append(year)
                activityRegisterPageBinding.dateOfBirth.text = date
            }, mYear, mMonth, mDay)
            datePickerDialog.show()
        }


        activityRegisterPageBinding.save.setOnClickListener {

            if (activityRegisterPageBinding.name.text!!.trim().isEmpty())
                makeToast(resources.getString(R.string.patient_name_error_meaasge))
            else if (activityRegisterPageBinding.email.text!!.trim().isEmpty())
                makeToast(resources.getString(R.string.email_empty_error_message))
            else if (!Patterns.EMAIL_ADDRESS.matcher(activityRegisterPageBinding.email.text!!.trim()).matches())
                makeToast(resources.getString(R.string.email_invalid_error_message))
            else if (activityRegisterPageBinding.password.text!!.trim().isEmpty())
                makeToast(resources.getString(R.string.password_empty_error_message))
            else if (activityRegisterPageBinding.countryspinnear.text!!.trim().isEmpty())
                makeToast(resources.getString(R.string.address_empty_error_message))
            else if (activityRegisterPageBinding.currencyspinnear.text!!.trim().isEmpty())
                makeToast(resources.getString(R.string.currency_empty_error_message))
            else if (activityRegisterPageBinding.mobileNumber.text!!.trim().isEmpty())
                makeToast(resources.getString(R.string.phone_empty_error_message))
            else {
                if (loginType != null && loginType == resources.getString(R.string.patient))
                {
                    if (gender.isEmpty())
                        makeToast(resources.getString(R.string.gender_empty_error_message))
                    else if (activityRegisterPageBinding.dateOfBirth.text!!.isEmpty())
                        makeToast(resources.getString(R.string.dob_empty_error_message))
                    else if (activityRegisterPageBinding.age.text!!.trim().isEmpty())
                        makeToast(resources.getString(R.string.age_empty_error_message))
                    else if (bloodGroup.isEmpty())
                        makeToast(resources.getString(R.string.blood_group_empty_error_message))
                    else {
                        if (gpsUserPermission.isConnectedInternet()) {
                            if (dialog == null)
                                showDialog()
                            else
                                dialog!!.show()
                            registerPageViewModel.getPatientRegistrationResponse(
                                activityRegisterPageBinding.name.text.toString().trim(),
                                activityRegisterPageBinding.email.text.toString().trim(),
                                activityRegisterPageBinding.password.text.toString().trim(),
                                activityRegisterPageBinding.address.text.toString().trim(),
                                activityRegisterPageBinding.mobileNumber.text.toString().trim(),
                                gender,
                                activityRegisterPageBinding.dateOfBirth.text.toString(),
                                activityRegisterPageBinding.age.text.toString().trim(),
                                bloodGroup
                            ).observe(this, Observer {
                                when (it.status) {
                                    Status.SUCCESS -> {
                                        if (it.data != null)
                                            logIn()
                                    }
                                    Status.ERROR -> {
                                        makeToast(it.message.toString())
                                        dialog!!.dismiss()
                                    }
                                    else -> {
                                        dialog!!.dismiss()
                                        makeToast(resources.getString(R.string.something_went_wrong))
                                    }
                                }
                            })
                        } else makeToast(resources.getString(R.string.no_internet_connection))
                    }
                } else if (loginType != null && loginType == resources.getString(R.string.nurse))
                {
                    if (activityRegisterPageBinding.registrationCertificateNumber.text!!.trim().isEmpty())
                        makeToast(resources.getString(R.string.registration_number_error_message))
                    else {
                        if (gpsUserPermission.isConnectedInternet()) {
                            if (dialog == null)
                                showDialog()
                            else
                                dialog!!.show()
                            registerPageViewModel.getNurseRegistrationResponse(
                                activityRegisterPageBinding.name.text.toString().trim(),
                                activityRegisterPageBinding.email.text.toString().trim(),
                                activityRegisterPageBinding.password.text.toString().trim(),
                                activityRegisterPageBinding.address.text.toString().trim(),
                                activityRegisterPageBinding.mobileNumber.text.toString().trim(),
                                activityRegisterPageBinding.registrationCertificateNumber.text.toString()
                                    .trim(),
                                "Approved"
                            ).observe(this, Observer {
                                when (it.status) {
                                    it.status -> {
                                        if (it.data != null)
                                            logIn()
                                    }
                                    Status.ERROR -> {
                                        makeToast(it.message.toString())
                                        dialog!!.dismiss()
                                    }
                                    else -> {
                                        dialog!!.dismiss()
                                        makeToast(resources.getString(R.string.something_went_wrong))
                                    }
                                }
                            })
                        } else makeToast(resources.getString(R.string.no_internet_connection))
                    }

                }
                else if (loginType != null && loginType == resources.getString(R.string.doctor))
                {
                    if (activityRegisterPageBinding.registrationCertificateNumber.text!!.trim().isEmpty())
                        makeToast(resources.getString(R.string.registration_number_error_message))
                    else {
                        if (gpsUserPermission.isConnectedInternet()) {
                            if (dialog == null)
                                showDialog()
                            else
                                dialog!!.show()
                            registerPageViewModel.getDoctorRegistrationResponse(
                                activityRegisterPageBinding.name.text.toString().trim(),
                                activityRegisterPageBinding.email.text.toString().trim(),
                                activityRegisterPageBinding.password.text.toString().trim(),
                                activityRegisterPageBinding.address.text.toString().trim(),
                                activityRegisterPageBinding.mobileNumber.text.toString().trim(),
                                activityRegisterPageBinding.registrationCertificateNumber.text.toString()
                                    .trim(),
                                department,
                                activityRegisterPageBinding.profile.text.toString().trim(),
                                activityRegisterPageBinding.facebookProfileLink.text.toString()
                                    .trim(),
                                activityRegisterPageBinding.twitterProfileLink.text.toString()
                                    .trim(),
                                activityRegisterPageBinding.googlePlusLink.text.toString().trim(),
                                activityRegisterPageBinding.linkedInProfileLink.text.toString()
                                    .trim(),
                                "Approved"
                            ).observe(this, Observer {
                                when (it.status) {
                                    it.status -> {
                                        if (it.data != null)
                                            logIn()
                                    }
                                    Status.ERROR -> {
                                        makeToast(it.message.toString())
                                        dialog!!.dismiss()
                                    }
                                    else -> {
                                        dialog!!.dismiss()
                                        makeToast(resources.getString(R.string.something_went_wrong))
                                    }
                                }
                            })
                        } else makeToast(resources.getString(R.string.no_internet_connection))
                    }
                }
            }


        }

        activityRegisterPageBinding.profileImage.setOnClickListener { showGalleryDialog();imageType = imageTypeList[0] }
        activityRegisterPageBinding.medicalDocument.setOnClickListener { showGalleryDialog(); imageType = imageTypeList[1] }
    }

    private fun makeToast(message: String) {
        Toast.makeText(this@RegisterPage, message, Toast.LENGTH_SHORT).show()
    }

    private fun showDialog() {
        progressDialogLayoutBinding = ProgressDialogLayoutBinding.inflate(LayoutInflater.from(this))
        progressDialogLayoutBinding.messageTxt.visibility = View.GONE
        dialog = AlertDialog.Builder(this).setView(progressDialogLayoutBinding.root).show()
        dialog!!.setCancelable(false)
    }

    private fun showGalleryDialog() {
        if (galleryPickerDialog == null) {
            galleryPickerLayoutBinding = GalleryPickerLayoutBinding.inflate(LayoutInflater.from(this))
            galleryPickerDialog = AlertDialog.Builder(this).setView(galleryPickerLayoutBinding.root).show()
            galleryPickerDialog!!.window!!.setWindowAnimations(R.style.DialogAnimation)
            galleryPickerDialog!!.setCancelable(true)
            galleryPickerLayoutBinding.gallery.setOnClickListener {
                showGalleryDialog()
                imageChooseType = true
                if (gpsUserPermission.readExternalStoragePermission())
                    startActivityForResult.launch(
                        Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                        )
                    )
                else
                    gpsUserPermission.requestUserPermission()
            }
            galleryPickerLayoutBinding.camera.setOnClickListener {
                showGalleryDialog()
                imageChooseType = false
                if (gpsUserPermission.cameraPermission())
                    startActivityForResult.launch(Intent(MediaStore.ACTION_IMAGE_CAPTURE))
                else
                    gpsUserPermission.requestUserPermission()
            }
            galleryPickerLayoutBinding.cancel.setOnClickListener { showGalleryDialog() }
        } else if (galleryPickerDialog!!.isShowing)
            galleryPickerDialog!!.dismiss()
        else
            galleryPickerDialog!!.show()

    }

    private val startActivityForResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if (it.resultCode == Activity.RESULT_OK && imageChooseType) {
            val imageUri: Uri = it.data!!.data!!
            val imageStream: InputStream? = contentResolver.openInputStream(imageUri)
            if (imageType == imageTypeList[0])
                activityRegisterPageBinding.profileImage.setImageBitmap(
                    BitmapFactory.decodeStream(
                        imageStream
                    )
                )
            else if (imageType == imageTypeList[1])
                activityRegisterPageBinding.medicalDocument.setImageBitmap(
                    BitmapFactory.decodeStream(
                        imageStream
                    )
                )
            else if (imageType == imageTypeList[2])
            {
                val imagename = getFileName(imageUri)
                activityRegisterPageBinding.chooseidcard.text = imagename
            }


        } else if (it.resultCode == Activity.RESULT_OK) {
            val thumbnail = it.data!!.extras!!["data"] as Bitmap?
            val bytes = ByteArrayOutputStream()
            thumbnail!!.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
            val destination = File(
                this.getExternalFilesDir(null)?.absolutePath,
                System.currentTimeMillis().toString() + ".jpg"
            )
            val file: FileOutputStream
            try {
                destination.createNewFile()
                file = FileOutputStream(destination)
                file.write(bytes.toByteArray())
                file.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            if (imageType == imageTypeList[0])
                activityRegisterPageBinding.profileImage.setImageBitmap(thumbnail)
            else if (imageType == imageTypeList[1])
                activityRegisterPageBinding.medicalDocument.setImageBitmap(thumbnail)
            else if (imageType == imageTypeList[2])
            {
                activityRegisterPageBinding.chooseidcard.text = System.currentTimeMillis().toString() + ".jpg"
            }


        }
    }

    private fun logIn() {
        dialog!!.dismiss()
        makeToast(resources.getString(R.string.successfully_registered))
        startActivity(Intent(this, LoginPage::class.java))
        finish()
    }

    fun getFileName(uri: Uri): String? {
        var result: String? = null
        if (uri.scheme == "content") {
            val cursor = contentResolver.query(uri, null, null, null, null)
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                }
            } finally {
                cursor!!.close()
            }
        }
        if (result == null) {
            result = uri.path
            val cut = result!!.lastIndexOf('/')
            if (cut != -1) {
                result = result.substring(cut + 1)
            }
        }
        return result
    }
}