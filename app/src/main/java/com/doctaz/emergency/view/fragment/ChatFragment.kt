package com.doctaz.emergency.view.fragment

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.doctaz.emergency.GpsUserPermission
import com.doctaz.emergency.R
import com.doctaz.emergency.adapter.RecentChatAdapter
import com.doctaz.emergency.databinding.FragmentChatBinding
import com.doctaz.emergency.room.ChatTable
import com.doctaz.emergency.view.activity.ChatPage
import com.doctaz.emergency.view.activity.SessionManager
import com.doctaz.emergency.viewmodel.FragmentChatViewModel
import dagger.hilt.android.AndroidEntryPoint
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import javax.inject.Inject

@AndroidEntryPoint
class ChatFragment: Fragment() {
    private lateinit var fragmentChatFragment: FragmentChatBinding
    private lateinit var fragmentChatViewModel: FragmentChatViewModel
    private lateinit var recentChatAdapter: RecentChatAdapter

    @Inject
    lateinit var sessionManager: SessionManager

    @Inject
    lateinit var gpsUserPermission: GpsUserPermission

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentChatFragment = DataBindingUtil.inflate(inflater, R.layout.fragment_chat, container, false)
        fragmentChatViewModel = ViewModelProvider(this).get(FragmentChatViewModel::class.java)

        init()

        return fragmentChatFragment.root
    }

    private fun init() {

        fragmentChatFragment.recentChatRecycler.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        fragmentChatFragment.recentChatRecycler.setHasFixedSize(true)

        findUniqueGroup()

    }


    private fun findUniqueGroup() {
        fragmentChatViewModel.getUniqueGroup().observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                if (fragmentChatFragment.noChatTxt.visibility == View.VISIBLE)
                    fragmentChatFragment.noChatTxt.visibility = View.GONE
                recentChatAdapter = RecentChatAdapter(it as ArrayList)
                fragmentChatFragment.recentChatRecycler.adapter = recentChatAdapter
                recentChatAdapter.notifyDataSetChanged()
                recentChatAdapter.setOnItemClickListener(object : RecentChatAdapter.RecentChatOnItemClickListener {
                    override fun onItemClick(position: Int, groupId: String, receiverName: String, lastSeen: String, imageUrl: String, receiverId: String, receiverType: String) {
                        fragmentChatViewModel.makeChatReadStatus(groupId)
                        startActivity(Intent(activity, ChatPage::class.java).putExtra("groupId", groupId).putExtra("userName", receiverName).putExtra("lastSeen", lastSeen).putExtra("imageUrl", imageUrl).putExtra("receiverId", receiverId).putExtra("receiverType", receiverType).putExtra("Tag", "Chat_Fragment"))
                    }
                }) //adapter end
            } else fragmentChatFragment.noChatTxt.visibility = View.VISIBLE
        })  //Observer end
    }

    private fun makeToast(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

    override fun onResume() {
        super.onResume()
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this)
        findUniqueGroup()
    }


    override fun onPause() {
        super.onPause()
        EventBus.getDefault().unregister(this)
    }


    override fun onDestroy() {
        super.onDestroy()
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: ArrayList<ChatTable>) {
        //fragmentChatViewModel.insertData(event)
        findUniqueGroup()
        /*val size = event.size - 1
        val groupId = event[size].messageGroupId
        val receiverId = event[size].messageSender
        val userName = event[size].messageSenderName
        val receiverType = event[size].messageSenderType
        val imageUrl = event[size].messageSenderImage
        val notificationId = 1000
        val builder: NotificationCompat.Builder = NotificationCompat.Builder(requireActivity(), "1").setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
            .setContentTitle(event[event.size - 1].messageSenderName)
            .setContentText(event[event.size - 1].messageDesc)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
            .setContentIntent(PendingIntent.getActivities(activity, 0, arrayOf(Intent(activity, ChatPage::class.java).putExtra("groupId", groupId).putExtra("receiverId", receiverId).putExtra("userName", userName).putExtra("receiverType", receiverType).putExtra("imageUrl", imageUrl).putExtra("notificationId", notificationId)), PendingIntent.FLAG_CANCEL_CURRENT))
            .setAutoCancel(true)
            .setOngoing(false)
            .setVibrate(longArrayOf(1000, 1000))
            .setDefaults(Notification.FLAG_ONLY_ALERT_ONCE)
        val manager = requireActivity().getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        manager.notify(notificationId, builder.build())*/

    }

}