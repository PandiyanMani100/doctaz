package com.doctaz.emergency.view.fragment

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.doctaz.emergency.GpsUserPermission
import com.doctaz.emergency.R
import com.doctaz.emergency.Status
import com.doctaz.emergency.databinding.FragmentProfileBinding
import com.doctaz.emergency.databinding.GalleryPickerLayoutBinding
import com.doctaz.emergency.databinding.ProgressDialogLayoutBinding
import com.doctaz.emergency.view.activity.SessionManager
import com.doctaz.emergency.viewmodel.ProfileViewModel
import com.doctaz.emergency.viewmodel.VideoCallViewModel

import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_profile.*
import java.io.*
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class ProfileFragment : Fragment() {
    private lateinit var galleryPickerLayoutBinding: GalleryPickerLayoutBinding
    private lateinit var progressDialogLayoutBinding: ProgressDialogLayoutBinding
    private lateinit var fragmentProfileBinding: FragmentProfileBinding
    private lateinit var profileViewModel: ProfileViewModel
    private lateinit var videocallviewModel: VideoCallViewModel

     var current_balance:String?=""
    private var dialog: AlertDialog? = null
    private var galleryPickerDialog: AlertDialog? = null
    private var imageChooseType = false
    private val bloodGroup = arrayOf("Select Blood Group", "A+", "A-", "B+", "B-", "AB+", "AB-", "O+", "O-")

    @Inject
    lateinit var gpsUserPermission: GpsUserPermission

    @Inject
    lateinit var sessionManager: SessionManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentProfileBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false)
        profileViewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)
        videocallviewModel = ViewModelProvider(this).get(VideoCallViewModel::class.java)
//        wallets()
        init()

        return fragmentProfileBinding.root
    }

    private fun init() {
        if (gpsUserPermission.isConnectedInternet()) {
            progressDialogLayoutBinding = ProgressDialogLayoutBinding.inflate(LayoutInflater.from(requireActivity()))
            dialog = AlertDialog.Builder(requireActivity()).setView(progressDialogLayoutBinding.root).show()
            dialog!!.window?.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT)
            progressDialogLayoutBinding.messageTxt.visibility = View.GONE
            dialog!!.setCancelable(false)
            setGenderStatus(0)
            setBloodGroup(0)
            profileViewModel.getProfileData().observe(viewLifecycleOwner, Observer {
                when (it.status) {
                    Status.SUCCESS -> {
                        if (dialog!!.isShowing)
                            dialog!!.dismiss()
                        Log.e("res",it.data.toString())
                        if (it.data!!.imageUrl!=null && it.data.imageUrl.isNotEmpty())
                            Glide.with(this).load(it.data.imageUrl).placeholder(R.drawable.profile_placeholder).into((fragmentProfileBinding.profileImage))
                        fragmentProfileBinding.name.setText(it.data.name)
                        fragmentProfileBinding.email.setText(it.data.email)
                        fragmentProfileBinding.address.setText(it.data.address)
                        fragmentProfileBinding.mobileNumber.setText(it.data.phone)
                        fragmentProfileBinding.dateOfBirth.setText(it.data.birthDate)
                        fragmentProfileBinding.age.setText(it.data.age)
                        if (it.data.sex.equals("male", ignoreCase = true))
                            setGenderStatus(1)
                        else
                            setGenderStatus(2)
                        for (item in bloodGroup.indices) {
                            if (it.data.bloodGroup.equals(bloodGroup[item],ignoreCase = true)) {
                                setBloodGroup(item)
                                break
                            }
                        }
                        wallets()

                    }
                    Status.ERROR -> resources.getString(R.string.invalid)
                    else -> resources.getString(R.string.something_went_wrong)
                }
            })
        } else
            makeToast(resources.getString(R.string.no_internet_connection))







        fragmentProfileBinding.calenderIcon.setOnClickListener {
            val date = StringBuffer()
            val c = Calendar.getInstance()
            val mYear = c[Calendar.YEAR]
            val mMonth = c[Calendar.MONTH]
            val mDay = c[Calendar.DAY_OF_MONTH]

            val datePickerDialog = DatePickerDialog(requireActivity(), { v, year, monthOfYear, dayOfMonth ->
                if (dayOfMonth.toString().length == 1)
                    date.append("0").append(dayOfMonth.toString()).append("/")
                else
                    date.append(dayOfMonth.toString()).append("/")
                if (monthOfYear.toString().length == 1)
                    date.append("0").append(monthOfYear + 1).append("/").append(year)
                else
                    date.append(monthOfYear + 1).append("/").append(year)
                fragmentProfileBinding.dateOfBirth.text = date
            }, mYear, mMonth, mDay)
            datePickerDialog.show()
        }

        fragmentProfileBinding.profileImage.setOnClickListener { showGalleryDialog() }
        fragmentProfileBinding.updatePassword.setOnClickListener { makeToast(resources.getString(R.string.implemetation_proress)) }
        fragmentProfileBinding.updateProfile.setOnClickListener { makeToast(resources.getString(R.string.implemetation_proress)) }

    }




    private fun wallets(){
        videocallviewModel.geWalletsdetailsResponse(sessionManager.getLoginId(),sessionManager.getLoginType()).observe(requireActivity(), Observer {
            when (it.status) {
                it.status -> {
                    if (it.data != null){
                        current_balance=""+it.data[0].currentBalance
                        tv_wallets.text="$ "+current_balance
                        sessionManager.setWalletCurrentAmount(it.data[0].currentBalance)

                    }

                }
                Status.ERROR -> {
                    makeToast(it.message.toString())
                    dialog!!.dismiss()
                }
                else -> {
                    dialog!!.dismiss()
                    makeToast(resources.getString(R.string.something_went_wrong))
                }
            }
        })
    }



    private fun makeToast(message: String) {
        Toast.makeText(requireActivity(), message, Toast.LENGTH_SHORT).show()
    }

    private fun setGenderStatus(position: Int) {
        val gender = arrayOf("Select Sex", "Male", "Female")
        val adapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, gender)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        fragmentProfileBinding.spinner.adapter = adapter
        fragmentProfileBinding.spinner.setSelection(position)

    }

    private fun setBloodGroup(position: Int) {
        val adapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, bloodGroup)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        fragmentProfileBinding.bloodGroup.adapter = adapter
        fragmentProfileBinding.bloodGroup.setSelection(position)
    }


    private val startActivityForResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if (it.resultCode == Activity.RESULT_OK && imageChooseType) {
            val imageUri: Uri = it.data!!.data!!
            val imageStream: InputStream? = requireContext().contentResolver.openInputStream(imageUri)
            fragmentProfileBinding.profileImage.setImageBitmap(BitmapFactory.decodeStream(imageStream))
        } else if (it.resultCode == Activity.RESULT_OK) {
            val thumbnail = it.data!!.extras!!["data"] as Bitmap?
            val bytes = ByteArrayOutputStream()
            thumbnail!!.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
            val destination = File(requireActivity().getExternalFilesDir(null)!!.absolutePath, System.currentTimeMillis().toString() + ".jpg")
            val file: FileOutputStream
            try {
                destination.createNewFile()
                file = FileOutputStream(destination)
                file.write(bytes.toByteArray())
                file.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            fragmentProfileBinding.profileImage.setImageBitmap(thumbnail)
        }
    }

    private fun showGalleryDialog() {
        if (galleryPickerDialog == null) {
            galleryPickerLayoutBinding = GalleryPickerLayoutBinding.inflate(LayoutInflater.from(requireActivity()))
            galleryPickerDialog = AlertDialog.Builder(requireActivity()).setView(galleryPickerLayoutBinding.root).show()
            galleryPickerDialog!!.window!!.setWindowAnimations(R.style.DialogAnimation)
            galleryPickerDialog!!.setCancelable(true)
            galleryPickerLayoutBinding.gallery.setOnClickListener {
                showGalleryDialog()
                imageChooseType = true
                if (gpsUserPermission.readExternalStoragePermission())
                    startActivityForResult.launch(Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI))
                else
                    gpsUserPermission.requestUserPermission()
            }
            galleryPickerLayoutBinding.camera.setOnClickListener {
                showGalleryDialog()
                imageChooseType = false
                if (gpsUserPermission.cameraPermission())
                    startActivityForResult.launch(Intent(MediaStore.ACTION_IMAGE_CAPTURE))
                else
                    gpsUserPermission.requestUserPermission()
            }
            galleryPickerLayoutBinding.cancel.setOnClickListener { showGalleryDialog() }
        } else if (galleryPickerDialog!!.isShowing)
            galleryPickerDialog!!.dismiss()
        else
            galleryPickerDialog!!.show()
    }


}