package com.doctaz.emergency.view.activity

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.doctaz.emergency.GpsUserPermission
import com.doctaz.emergency.R
import com.doctaz.emergency.Status
import com.doctaz.emergency.adapter.DoctorsListAdapter
import com.doctaz.emergency.adapter.DoctorsTypeAdapter
import com.doctaz.emergency.databinding.ActivityDoctorsListBinding
import com.doctaz.emergency.model.DoctorsListResponse
import com.doctaz.emergency.model.DoctorsTypeResponse
import com.doctaz.emergency.viewmodel.DoctorsListViewModel
import com.doctaz.emergency.viewmodel.VideoCallViewModel

import com.google.android.material.bottomsheet.BottomSheetDialog
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_doctors_list.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList


@AndroidEntryPoint
class DoctorsList : AppCompatActivity() {
    private lateinit var doctorsListBinding: ActivityDoctorsListBinding
    private lateinit var doctorsListViewModel: DoctorsListViewModel
    private lateinit var doctorsListViewModelgroupoid: DoctorsListViewModel
    private lateinit var videocallviewModel: VideoCallViewModel
    private lateinit var doctorsListAdapter: DoctorsListAdapter
    private var doctorsTypeAdapter: DoctorsTypeAdapter? = null
    private lateinit var doctorsList: DoctorsListResponse
    var doctorsType: DoctorsTypeResponse? = null
    private var aInflater: LayoutInflater? = null
    private var onlineList = ArrayList<DoctorsList>()
    private var dialog2: BottomSheetDialog? = null
    @Inject
    lateinit var userPermission: GpsUserPermission

    @Inject
    lateinit var sessionManager: SessionManager

    var taitle: String? = ""
    var receiver_Id: String? = ""
    var group_id = ""
    var status = ""
    var id_type = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        doctorsListBinding = DataBindingUtil.setContentView(this, R.layout.activity_doctors_list)
        doctorsListViewModel = ViewModelProvider(this).get(DoctorsListViewModel::class.java)
        videocallviewModel = ViewModelProvider(this).get(VideoCallViewModel::class.java)
        taitle = intent.getStringExtra("taitle")
        aInflater = LayoutInflater.from(this@DoctorsList)


        init()
    }

    private fun init() {

        doctorsListBinding.filter.setOnClickListener {

            val dialogView = aInflater!!.inflate(R.layout.dialog, null)
            val dialog = BottomSheetDialog(this@DoctorsList)
            dialog.setContentView(dialogView)
            val show_online = dialogView.findViewById<View>(R.id.show_online) as TextView
            val show_all = dialogView.findViewById<View>(R.id.show_all) as TextView
            val high_to_low = dialogView.findViewById<View>(R.id.high_to_low) as TextView
            val low_to_high = dialogView.findViewById<View>(R.id.low_to_high) as TextView
            val types = dialogView.findViewById<View>(R.id.types) as TextView
            val view = dialogView.findViewById<View>(R.id.view)
            dialog.show()


            high_to_low.setOnClickListener {
                dialog.dismiss()
            }

            low_to_high.setOnClickListener {
                dialog.dismiss()
            }

            show_online.setOnClickListener {

                val listonline = DoctorsListResponse()
                for (s: DoctorsListResponse.DoctorsList in doctorsList) {
                    if (s.onlineStatus.equals("1")) {
                        listonline.add(s)
                    }
                }
                doctorsListAdapter.updateList(listonline)
                dialog.dismiss()
            }

            show_all.setOnClickListener {

                val listall = DoctorsListResponse()
                for (s: DoctorsListResponse.DoctorsList in doctorsList) {
                    listall.add(s)
                }
                doctorsListAdapter.updateList(listall)
                dialog.dismiss()
            }

            types.setOnClickListener {
                dialog.dismiss()

                val dialogView = aInflater!!.inflate(R.layout.dialog_2, null)
                dialog2 = BottomSheetDialog(this@DoctorsList)
                dialog2!!.setContentView(dialogView)
                dialog2!!.show()
                val doctorslistType = dialogView.findViewById<View>(R.id.doctorslistType) as RecyclerView
                getdoctorsType(doctorslistType)
            }


        }




        doctorsList = DoctorsListResponse()
        doctorsListBinding.shimmerViewContainer.startShimmerAnimation()

        if (userPermission.isConnectedInternet()) {
            getDoctorList()
        } else
            makeToast(resources.getString(R.string.no_internet_connection))

        doctorsListBinding.departmentList.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        doctorsListAdapter = DoctorsListAdapter(this, doctorsList)
        doctorsListBinding.departmentList.adapter = doctorsListAdapter

        doctorsListBinding.searchDoctor.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                updateDoctorsList(s.toString().toLowerCase(Locale.ROOT))
            }
        })

        doctorsListBinding.imageView.setOnClickListener { finish() }

        doctorsListAdapter.setOnItemClickListener(object : DoctorsListAdapter.DoctorsListOnItemClickListener {
            override fun onItemClick(position: Int, receiverId: String, receiverType: String, receiverName: String, imageUrl: String, eventType: String) {

                if (userPermission.isConnectedInternet()) {
                    if (eventType == resources.getString(R.string.chat_taxt)) {
                        receiver_Id = "" + receiverId
                        chat(position, receiver_Id!!, receiverType, receiverName, imageUrl, eventType)

                    } else if (eventType == resources.getString(R.string.video_txt)) {

                        Walletsdetails(receiverId, receiverName, imageUrl)
                        alertShow(receiverId, receiverName, imageUrl)


                    } else if (eventType == resources.getString(R.string.call_txt))
                        makeToast(resources.getString(R.string.implemetation_proress))

                } else
                    makeToast(resources.getString(R.string.no_internet_connection))

            }
        })


    }


    fun alertShow(receiverId: String, receiverName: String, imageUrl: String) {
        val dialog = Dialog(this, R.style.theme_dialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.alert_videocall_layout)
        val body = dialog.findViewById(R.id.tvBody) as TextView
        val cancel = dialog.findViewById(R.id.cancel) as TextView
        val iv_cancel = dialog.findViewById(R.id.iv_cancel) as ImageView
        val conformation = dialog.findViewById(R.id.conformation) as TextView
        val tv_addamount = dialog.findViewById(R.id.tv_addamount) as TextView
        val ed_duration = dialog.findViewById(R.id.ed_duration) as EditText
        var duration = ed_duration.text.toString()
        var time = ""
        var product: String? = ""
        tv_addamount.text = "$ " + sessionManager.getWalletCurrentAmount()

        ed_duration.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                time = s.toString()

                if (time.equals("")) {
                    time = "0.0"
                    product = "" + 2 * time.toFloat()
                    conformation.text = "Proceed To Video Call $ " + product.toString()
                } else {
                    product = "" + 2 * time.toFloat()
                    conformation.text = "Proceed To Video Call $ " + product.toString()
                }
            }

        })

        conformation.text = "Proceed To Video Call"

        conformation.setOnClickListener {


            if (product!!.equals("")) {
                makeToast("Please set the time")

            } else {
                if (product!!.toFloat() <= sessionManager.getWalletCurrentAmount().toFloat()) {
                    startActivity(Intent(this@DoctorsList, PublishVideoCall::class.java).putExtra("receiverId", receiverId).putExtra("userName", receiverName).putExtra("receiverType", "doctor").putExtra("imageUrl", imageUrl).putExtra("call_minutes", time).putExtra("call_amount", "" + product!!.toFloat()))
                    dialog.dismiss()
                } else {
                    makeToast("Wallet Amount too less make the call")

                }


            }


        }
        cancel.setOnClickListener { dialog.dismiss() }
        dialog.show()
        iv_cancel.setOnClickListener { dialog.dismiss() }
        dialog.show()

    }


    fun Walletsdetails(receiverId: String, receiverName: String, imageUrl: String) {
        videocallviewModel.geWalletsdetailsResponse(sessionManager.getLoginId(), sessionManager.getLoginType()).observe(this, Observer {
            when (it.status) {
                it.status -> {
                    if (it.data != null) {
                        sessionManager.setWalletCurrentAmount(it.data[0].currentBalance)

                    }
                }
                Status.ERROR -> {
                    makeToast(it.message.toString())
                }
                else -> {
                    makeToast(resources.getString(R.string.something_went_wrong))
                }
            }
        })
    }


    fun chat(position: Int, receiverId: String, receiverType: String, receiverName: String, imageUrl: String, eventType: String) {

        doctorsListViewModelgroupoid = ViewModelProvider(this).get(DoctorsListViewModel::class.java)

        doctorsListViewModelgroupoid.createInboxBoxGroupId(sessionManager.getLoginId(), sessionManager.getLoginType(), receiverId, resources.getString(R.string.doctor)).observe(this@DoctorsList, Observer {
            when (it.status) {
                Status.SUCCESS -> if (it.status!!.equals(Status.SUCCESS)) {
                    if (it.data != null) {
                        if (it.data!!.messageGroupId != null) {
                            group_id = "" + it.data!!.messageGroupId!!
                            status = "" + Status.SUCCESS

                        }
                    } else {
                        group_id = ""
                        status = ""
                    }


                } else {
                    group_id = ""
                    status = ""

                }

                Status.ERROR -> makeToast(resources.getString(R.string.invalid))
                else -> makeToast(resources.getString(R.string.something_went_wrong))
            }
        })

        if (status.equals("SUCCESS")) {
            startActivity(Intent(this@DoctorsList, ChatPage::class.java).putExtra("groupId", group_id).putExtra("receiverId", receiverId).putExtra("userName", receiverName).putExtra("receiverType", "doctor").putExtra("imageUrl", imageUrl).putExtra("Tag", "Doctors_list"))
            finish()
        }


    }

    override fun onResume() {
        super.onResume()
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun doThis(event: String) {
        if (event.startsWith("chat,")) {

            val values: List<String> = event.split(",")
            var id_typeValue = values[1].toInt()
            id_type = "" + id_typeValue
            val listonline = DoctorsListResponse()
            for (s: DoctorsListResponse.DoctorsList in doctorsList) {
                if (s.departmentId.equals("" + id_type)) {
                    listonline.add(s) }
            }
           if (listonline.size.equals(0)){
               doctorsListBinding.departmentList.visibility = View.GONE
               no_departmentList.visibility = View.VISIBLE
           }else{
               doctorsListBinding.departmentList.visibility = View.VISIBLE

               no_departmentList.visibility = View.GONE

           }
            doctorsListAdapter.updateList(listonline)
            dialog2!!.dismiss()


        }
    }

    private fun updateDoctorsList(text: String) {
        val list = DoctorsListResponse()
        for (s: DoctorsListResponse.DoctorsList in doctorsList) {
            if (s.name.toLowerCase(Locale.ROOT).contains(text))
                list.add(s)
        }
        doctorsListAdapter.updateList(list)
    }

    private fun getDoctorList() {
        doctorsListViewModel.getLoginResponse().observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    doctorsList = it.data!!

                    no_departmentList.visibility = View.GONE

                    doctorsListAdapter.updateList(it.data)
                    doctorsListBinding.shimmerViewContainer.visibility = View.GONE
                    doctorsListBinding.departmentList.visibility = View.VISIBLE

                    if (doctorsList.size.equals(0)) {
                        no_departmentList.visibility = View.VISIBLE
                    }else{
                        no_departmentList.visibility = View.GONE

                    }
                    doctorsListBinding.shimmerViewContainer.stopShimmerAnimation()
                }
                Status.LOADING -> Toast.makeText(this@DoctorsList, it.data.toString(), Toast.LENGTH_SHORT).show()
                Status.ERROR -> Toast.makeText(this@DoctorsList, resources.getString(R.string.invalid), Toast.LENGTH_SHORT).show()
                Status.NOINTERNET -> Toast.makeText(this@DoctorsList, resources.getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show()
                else -> Toast.makeText(this@DoctorsList, resources.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show()
            }
        })
    }


    private fun getdoctorsType(doctorslistType: RecyclerView) {
        doctorsListViewModel.getdoctorsType().observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    doctorsType = it.data!!
                    doctorslistType.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
                    doctorsTypeAdapter = DoctorsTypeAdapter(this, doctorsType!!)
                    doctorslistType.adapter = doctorsTypeAdapter


                    doctorsListBinding.shimmerViewContainer.visibility = View.GONE
                    doctorsListBinding.departmentList.visibility = View.VISIBLE
                    doctorsListBinding.shimmerViewContainer.stopShimmerAnimation()
                }
                Status.LOADING -> Toast.makeText(this@DoctorsList, it.data.toString(), Toast.LENGTH_SHORT).show()
                Status.ERROR -> Toast.makeText(this@DoctorsList, resources.getString(R.string.invalid), Toast.LENGTH_SHORT).show()
                Status.NOINTERNET -> Toast.makeText(this@DoctorsList, resources.getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show()
                else -> Toast.makeText(this@DoctorsList, resources.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show()
            }
        })
    }


    private fun makeToast(message: String) {
        Toast.makeText(this@DoctorsList, message, Toast.LENGTH_SHORT).show()
    }
}
