package com.doctaz.emergency.view.activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.doctaz.emergency.R

class CallAlert : AppCompatActivity() {
    private var customTextView2:TextView? = null
    private var endCall:LinearLayout? = null
    private var acceptCall:LinearLayout? = null
    private var message:LinearLayout? = null
    private var mediaPlayer: MediaPlayer? = null
    private var sessionid:String  = ""
    private var sessionToken:String  = ""
    private var doctorname:String = ""
    private var userCallType: String = ""
    private var apiKey: String = ""


    @SuppressLint("InvalidWakeLockTag")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_call_alert)
        Log.e("JSON_OBJECT", "Wake Lock")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            setShowWhenLocked(true)
            setTurnScreenOn(true)
        } else {
            window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                    or WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                    or WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                    or WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                    or WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON)
        }
        customTextView2 = findViewById(R.id.customTextView2) as TextView
        endCall= findViewById(R.id.endCall) as LinearLayout
        acceptCall= findViewById(R.id.acceptCall) as LinearLayout

        message= findViewById(R.id.message) as LinearLayout

        val MAX_VOLUME = 100
        val volume = (1 - Math.log(MAX_VOLUME - 80.toDouble()) / Math.log(MAX_VOLUME.toDouble())).toFloat()
        mediaPlayer = MediaPlayer.create(this, R.raw.call_alert)
        val audioManager: AudioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 20, 0)
        try {
            mediaPlayer!!.setVolume(volume, volume)
        } catch (e: NullPointerException) {
        } catch (e: Exception) {
        }
        if (mediaPlayer != null) {
            if (!mediaPlayer!!.isPlaying) {
                mediaPlayer!!.start()
                mediaPlayer!!.isLooping = true
            }
        }

        sessionid = intent.getStringExtra("sessionId").toString()
        sessionToken = intent.getStringExtra("sessionToken").toString()
        doctorname = intent.getStringExtra("doctorName").toString()
        userCallType= intent.getStringExtra("callType").toString()
        apiKey=intent.getStringExtra("apiKey").toString()

        customTextView2!!.setText(doctorname)

        endCall!!.setOnClickListener {
            startcallapihit(sessionid,"decline")
            finish()
        }
        message!!.setOnClickListener {
            Toast.makeText(applicationContext,R.string.implemetation_proress,Toast.LENGTH_SHORT).show()
        }
        acceptCall!!.setOnClickListener {
            startcallapihit(sessionid,"accept")
            startActivity(Intent(this@CallAlert, PublishVideoCall::class.java).putExtra("sessionId",sessionid).putExtra("sessionToken",sessionToken).putExtra("userName",doctorname).putExtra("callType",userCallType).putExtra("apiKey",apiKey))
            finish()
        }
        onShakeImage()
        Handler(Looper.getMainLooper()).postDelayed({
           finish()
        }, 30000)

    }
    fun onShakeImage() {
        val shake: Animation
        shake = AnimationUtils.loadAnimation(applicationContext, R.anim.shake)
        shake.setRepeatCount(20);
        acceptCall!!.startAnimation(shake) // starts animation
    }

    override fun onDestroy() {
        if (mediaPlayer != null) {
            if (mediaPlayer!!.isPlaying()) {
               mediaPlayer!!.stop()
            }
        }
        super.onDestroy()
    }

    fun startcallapihit(roomnumber:String , typeofcall:String)
    {
        val cbIntent = Intent()
        cbIntent.setClass(this, callhitapis::class.java)
        cbIntent.putExtra("room_number", roomnumber)
        cbIntent.putExtra("typeofcall", typeofcall)
        startService(cbIntent)
    }
}