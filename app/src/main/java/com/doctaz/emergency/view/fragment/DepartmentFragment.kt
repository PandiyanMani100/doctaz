package com.doctaz.emergency.view.fragment

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.PopupMenu
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.doctaz.emergency.R
import com.doctaz.emergency.Status

import com.doctaz.emergency.adapter.DepartListOnItemClickListener
import com.doctaz.emergency.adapter.DepartmentListAdapter
import com.doctaz.emergency.databinding.FragmentDepartmentBinding
import com.doctaz.emergency.model.DashboardMainPojo
import com.doctaz.emergency.model.Department
import com.doctaz.emergency.view.activity.DoctorsList
import com.doctaz.emergency.view.activity.LoginPage
import com.doctaz.emergency.view.activity.SessionManager
import com.doctaz.emergency.viewmodel.DepartmentCountViewModel
import com.doctaz.emergency.viewmodel.DepartmentFragmentViewModel

import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

@AndroidEntryPoint
class DepartmentFragment : Fragment() , DepartListOnItemClickListener {
    private  lateinit var departmentFragment: FragmentDepartmentBinding
    private  lateinit var departmentListAdapter:DepartmentListAdapter
    private  var departmentFragmentViewModel: DepartmentFragmentViewModel?=null
    private  var departmentCountViewModel: DepartmentCountViewModel?=null
    private  var dashboardMainPojo: DashboardMainPojo?=null
    private var departListOnItemClickListener: DepartListOnItemClickListener? = null
    private  var main_list: ArrayList<Department> = ArrayList()
    private var display_list: ArrayList<Department> = ArrayList()


    @Inject
    lateinit var sessionManager: SessionManager


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        departmentFragment = DataBindingUtil.inflate(inflater, R.layout.fragment_department, container, false)
        departmentFragmentViewModel = ViewModelProvider(this).get(DepartmentFragmentViewModel::class.java)
        departmentCountViewModel = ViewModelProvider(this).get(DepartmentCountViewModel::class.java)
        init()
        getDoctorList()

        departListOnItemClickListener=this
       this.sessionManager
        return departmentFragment!!.root
    }

    private fun init() {


        departmentFragment!!.more.setOnClickListener {
            val popupMenu = PopupMenu(requireContext(), departmentFragment!!.more)
            popupMenu.menuInflater.inflate(R.menu.menu_list, popupMenu.menu)
            popupMenu.setOnMenuItemClickListener {
                departmentFragmentViewModel!!.deleteChatTable()
                sessionManager.setClear()
                startActivity(Intent(activity, LoginPage::class.java).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK))
                return@setOnMenuItemClickListener true
            }
            popupMenu.show()
        }

        departmentFragment!!.imageView2.setOnClickListener {
            requireActivity().finish()
        }

        departmentFragment.searchDoctorDepartment.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                display_list.clear()
                val search = s.toString().toLowerCase(Locale.getDefault())
              if(search.length!=0){
                  main_list.forEach {
                      if (it.title.toLowerCase(Locale.getDefault()).contains(search)) {
                          display_list.add(it)
                      }
                  }

                  if (display_list.isEmpty()) {
                      Toast.makeText(activity!!, "No match found", Toast.LENGTH_SHORT).show()
                  }
                  departmentFragment.departmentList.adapter!!.notifyDataSetChanged()
              }else{
                  display_list.clear()
                  display_list.addAll(main_list)
                departmentFragment.departmentList.adapter!!.notifyDataSetChanged()
              }



            }
            override fun afterTextChanged(s: Editable?) {
                //updateDoctorsList(s.toString().toLowerCase(Locale.ROOT))
            }
        })

       /* departmentListAdapter.setOnItemClickListener(object : DepartmentListAdapter.DepartListOnItemClickListener {
            override fun onItemClick(position: Int) {
                when (position) {
                    0 -> startActivity(Intent(activity, DoctorsList::class.java))
                    else -> Toast.makeText(activity, resources.getString(R.string.implemetation_proress), Toast.LENGTH_SHORT).show()
                }

            }
        })*/
    }




    private fun getDoctorList() {
        departmentCountViewModel!!.getDepartmentCount(sessionManager.getLoginType()).observe(requireActivity(), Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    dashboardMainPojo = it.data!!

                    departmentFragment!!.shimmerViewContainer.visibility = View.GONE
                    departmentFragment!!.shimmerViewContainer.stopShimmerAnimation()
                    main_list.addAll(dashboardMainPojo!!.department)
                    display_list.addAll(main_list)

                    departmentFragment.departmentList!!.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
                    departmentListAdapter = DepartmentListAdapter(
                            requireActivity(),
                            display_list,
                            this
                        )


                    departmentFragment.departmentList!!.adapter = departmentListAdapter
                    departmentFragment!!.departmentList.visibility = View.VISIBLE
//                    departmentFragment!!.shimmerViewContainer.stopShimmerAnimation()



                }
                Status.LOADING -> Toast.makeText(activity, it.data.toString(), Toast.LENGTH_SHORT).show()
                Status.ERROR -> Toast.makeText(activity, resources.getString(R.string.invalid), Toast.LENGTH_SHORT).show()
                Status.NOINTERNET -> Toast.makeText(activity, resources.getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show()
                else -> Toast.makeText(activity, resources.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show()
            }
        })
    }

    override fun onItemClick(position: Int,taitle:String) {

       if( taitle.equals("Doctor")){

           var intent =Intent(activity, DoctorsList::class.java)
           intent.putExtra("taitle",taitle)
           startActivity(intent)
       }else {

           Toast.makeText(activity, resources.getString(R.string.implemetation_proress), Toast.LENGTH_SHORT).show()
       }


    }


}

