package com.doctaz.emergency.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.doctaz.emergency.R
import com.doctaz.emergency.databinding.RecentChatRecyclerLayoutBinding
import com.doctaz.emergency.model.RecentMessageModel


class RecentChatAdapter(private var recentChatList: ArrayList<RecentMessageModel>) : RecyclerView.Adapter<RecentChatAdapter.ViewHolder>() {
    private lateinit var recentChatOnItemClickListener: RecentChatOnItemClickListener
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = RecentChatRecyclerLayoutBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }
    
    override fun onBindViewHolder(holder:ViewHolder, position: Int) {
        val dataModel = recentChatList[position]
        holder.binding.bind=dataModel
        Glide.with(holder.binding.profileImage.context).load(dataModel.imageUri).placeholder(R.drawable.doctor_image_girl).into(holder.binding.profileImage)
        holder.itemView.setOnClickListener{ recentChatOnItemClickListener.onItemClick(position,dataModel.groupId,dataModel.receiverName,dataModel.date,dataModel.imageUri,dataModel.receiverId,dataModel.receiverType)}

        if (recentChatList.size-1==position) holder.binding.view.visibility=View.GONE
        else holder.binding.view.visibility=View.VISIBLE

        if (recentChatList[position].count==0) holder.binding.unreadMessageCount.visibility=View.GONE
        else {
            holder.binding.unreadMessageCount.visibility=View.VISIBLE
            holder.binding.unreadMessageCount.text=dataModel.count.toString()
        }

    }

    override fun getItemCount(): Int {
        return recentChatList.size
    }
    
    fun setOnItemClickListener(recentChatOnItemClickListener: RecentChatOnItemClickListener){
        this.recentChatOnItemClickListener=recentChatOnItemClickListener
    }
    
    interface RecentChatOnItemClickListener{
        fun onItemClick(position: Int,groupId:String,receiverName:String,lastSeen:String,imageUrl:String,receiverId:String,receiverType:String)
    }


    class ViewHolder(var binding: RecentChatRecyclerLayoutBinding) : RecyclerView.ViewHolder(binding.root)
}