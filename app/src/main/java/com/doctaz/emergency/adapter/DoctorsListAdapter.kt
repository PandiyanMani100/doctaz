package com.doctaz.emergency.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.doctaz.emergency.R
import com.doctaz.emergency.databinding.DoctorListRecycleLayoutBinding
import com.doctaz.emergency.model.DoctorsListResponse
import kotlinx.android.synthetic.main.call_history_recycler_layout.view.*
import kotlinx.android.synthetic.main.doctor_list_recycle_layout.view.*
import org.greenrobot.eventbus.EventBus


class DoctorsListAdapter(var contax:Context,private var doctorsList: DoctorsListResponse) : RecyclerView.Adapter<DoctorsListAdapter.ViewHolder>() {
    private lateinit var doctorsListOnItemClickListener: DoctorsListOnItemClickListener

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataModel = doctorsList[position]
        holder.binding.list = dataModel



    /*    if (position == doctorsList.size - 1)
            holder.binding.view.visibility = View.GONE
        else
            holder.binding.view.visibility = View.VISIBLE*/

        if (doctorsList[position].onlineStatus == "1")
            holder.binding.doctorStatus.visibility = View.VISIBLE

        else
            holder.binding.doctorStatus.visibility = View.GONE



        if (doctorsList[position].imageUrl != null && doctorsList[position].imageUrl.isNotEmpty())
          //  Glide.with(contax).load(dataModel.imageUrl).into((holder.binding.profileImage))

        Glide.with(contax).load(doctorsList[position].imageUrl).centerCrop().into(holder.itemView.profileImage);
        holder.binding.video.setOnClickListener { doctorsListOnItemClickListener.onItemClick(position, dataModel.doctorId, "", dataModel.name, dataModel.imageUrl, holder.binding.video.resources.getString(
            R.string.video_txt)) }
        holder.binding.call.setOnClickListener { doctorsListOnItemClickListener.onItemClick(position, dataModel.doctorId, "", dataModel.name, dataModel.imageUrl, holder.binding.video.resources.getString(R.string.call_txt)) }
        holder.binding.chat.setOnClickListener {

            EventBus.getDefault().post("chat")

            doctorsListOnItemClickListener.onItemClick(position, dataModel.doctorId, "", dataModel.name, dataModel.imageUrl, holder.binding.video.resources.getString(R.string.chat_taxt)) }





    }

    fun setOnItemClickListener(navigationOnClickListener: DoctorsListOnItemClickListener) {
        this.doctorsListOnItemClickListener = navigationOnClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, p: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DoctorListRecycleLayoutBinding.inflate(layoutInflater, parent, false) //if parant and attachToRoot(false) not given else it take wrab-content
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        var size = 0
        try {
            size = doctorsList.size
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return size
    }

    interface DoctorsListOnItemClickListener {
        fun onItemClick(position: Int, receiverId: String, receiverType: String, receiverName: String, imageUrl: String, eventType: String)
    }

    fun updateList(doctorsList: DoctorsListResponse) {
        this.doctorsList = doctorsList
        notifyDataSetChanged()
    }

    class ViewHolder(var binding: DoctorListRecycleLayoutBinding) : RecyclerView.ViewHolder(binding.root)
}

