package com.doctaz.emergency.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.doctaz.emergency.R
import com.doctaz.emergency.model.Department

import kotlinx.android.synthetic.main.department_list_recycler_layout.view.*

class DepartmentListAdapter(var context:Context, private var department: List<Department>, var departListOnItemClickListener: DepartListOnItemClickListener) : RecyclerView.Adapter<DepartmentListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.department_list_recycler_layout, parent, false)
        )
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(department[position],position)
        holder.itemView.doctorName.text=""+department[position].title+"s"
        holder.itemView.doctorDesignation.text=""+department[position].total.toString()+" user"
        holder.itemView.onlineDoctorsCount.text=department[position].online.toString()

        if (department[position].title.equals("Nurse")){
            holder.itemView.profile_image.setImageResource(R.drawable.nurse_new)

        }

        holder.itemView.departmentmainlay.setOnClickListener {
            departListOnItemClickListener!!.onItemClick(position,department[position].title)
        }

    }
    override fun getItemCount(): Int {
        return department.size
    }
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(version: Department,postion: Int) {}}
}
/*fun setOnItemClickListener(navigationOnClickListener: DepartListOnItemClickListener) {
    this.departListOnItemClickListener = navigationOnClickListener
}*/

interface DepartListOnItemClickListener {
    fun onItemClick(position: Int,dep:String)
}