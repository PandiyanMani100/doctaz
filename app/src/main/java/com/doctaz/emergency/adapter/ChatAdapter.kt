package com.doctaz.emergency.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.doctaz.emergency.databinding.ReceiverLayoutBinding
import com.doctaz.emergency.databinding.SenderLayoutBinding
import com.doctaz.emergency.room.ChatTable
import java.io.File

class ChatAdapter(var chatArrayList: ArrayList<ChatTable>, var loginType: String) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val layoutViewTypeFirst = 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)

        if (viewType == layoutViewTypeFirst)
            return SenderViewHolder(SenderLayoutBinding.inflate(layoutInflater, parent, false))
        return ReceiverViewHolder(ReceiverLayoutBinding.inflate(layoutInflater, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (layoutViewTypeFirst == holder.itemViewType)
            (holder as ChatAdapter.SenderViewHolder).bind(position)
        else
            (holder as ChatAdapter.ReceiverViewHolder).bind(position)
    }

    inner class SenderViewHolder(private var senderLayoutBinding: SenderLayoutBinding) : RecyclerView.ViewHolder(senderLayoutBinding.root) {
        fun bind(position: Int) {
            if (chatArrayList[position].messageFile.isEmpty()) {

                val message = chatArrayList[position].messageDesc
                val text = "<font color=#000000>$message</font> <font color=#00000000>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font>"
                senderLayoutBinding.senderMessage.text =  HtmlCompat.fromHtml(text, HtmlCompat.FROM_HTML_MODE_LEGACY)
                senderLayoutBinding.sendTime.text = chatArrayList[position].messageDate
                senderLayoutBinding.textLayout.visibility = View.VISIBLE
                senderLayoutBinding.imageChatLayout.visibility = View.GONE
                if (chatArrayList[position].messageStatus == "read")
                    senderLayoutBinding.doubleTick.setColorFilter(Color.GREEN)
            } else if (chatArrayList[position].messageFile.isNotEmpty()){
                senderLayoutBinding.textLayout.visibility=View.GONE
                senderLayoutBinding.imageChatLayout.visibility=View.VISIBLE
                senderLayoutBinding.sendTime.text = chatArrayList[position].messageDate
                Glide.with(senderLayoutBinding.imageView.context).load(File(chatArrayList[position].messageFile)).into(senderLayoutBinding.imageView)
            }
        }
    }

    inner class ReceiverViewHolder(private var receiverLayoutBinding: ReceiverLayoutBinding) : RecyclerView.ViewHolder(receiverLayoutBinding.root) {
        fun bind(position: Int) {
            val message = chatArrayList[position].messageDesc
            val text = "<font color=#000000>$message</font> <font color=#00000000>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font>"
            receiverLayoutBinding.senderMessage.text =  HtmlCompat.fromHtml(text, HtmlCompat.FROM_HTML_MODE_LEGACY)
            receiverLayoutBinding.sendTime.text = chatArrayList[position].messageDate
            receiverLayoutBinding.textLayout.visibility = View.VISIBLE
            receiverLayoutBinding.imageChatLayout.visibility = View.GONE
        }
    }


    override fun getItemCount(): Int {
        return chatArrayList.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (loginType == chatArrayList[position].messageSenderType)
            1
        else
            2
    }

    fun updateList(chatArrayList: ArrayList<ChatTable>) {
        this.chatArrayList = chatArrayList
        notifyDataSetChanged()
    }
}