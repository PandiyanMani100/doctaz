package com.doctaz.emergency.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.doctaz.emergency.R
import com.doctaz.emergency.model.DoctorsTypeResponse
import kotlinx.android.synthetic.main.doctor_type_layout.view.*
import org.greenrobot.eventbus.EventBus


class DoctorsTypeAdapter(var contax:Context, private var doctorsList: DoctorsTypeResponse) : RecyclerView.Adapter<DoctorsTypeAdapter.ViewHolder>() {
    private lateinit var doctorsListOnItemClickListener: DoctorsListOnItemClickListener

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {



        if (doctorsList[position].id != null && doctorsList[position].id.isNotEmpty())
          //  Glide.with(contax).load(dataModel.imageUrl).into((holder.binding.profileImage))

            holder.itemView.doctorsType.text=""+doctorsList[position].name

        holder.itemView.doctorsType.setOnClickListener {

            EventBus.getDefault().post("chat,"+doctorsList[position].id)
            notifyDataSetChanged()

        }





    }

    fun setOnItemClickListener(navigationOnClickListener: DoctorsListOnItemClickListener) {
        this.doctorsListOnItemClickListener = navigationOnClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, p: Int): ViewHolder {
//        val layoutInflater = LayoutInflater.from(parent.context)

//        val binding = DoctorListRecycleLayoutBinding.inflate(layoutInflater, parent, false) //if parant and attachToRoot(false) not given else it take wrab-content

        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.doctor_type_layout, parent, false)

        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        var size = 0
        try {
            size = doctorsList.size
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return size
    }

    interface DoctorsListOnItemClickListener {
        fun onItemClick(position: Int, receiverId: String, receiverType: String, receiverName: String, imageUrl: String, eventType: String)
    }

    fun updateTypelist(doctorsList: DoctorsTypeResponse) {
        this.doctorsList = doctorsList
        notifyDataSetChanged()
    }

    class ViewHolder(var view:View) : RecyclerView.ViewHolder(view)
}

