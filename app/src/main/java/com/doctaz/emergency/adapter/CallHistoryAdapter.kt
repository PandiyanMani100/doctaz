package com.doctaz.emergency.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.doctaz.emergency.R
import com.doctaz.emergency.model.CallHistoryItem
import com.doctaz.emergency.view.activity.SessionManager
import kotlinx.android.synthetic.main.call_history_recycler_layout.view.*

class CallHistoryAdapter(var context:Context, private var department: List<CallHistoryItem>, var user_id: String) : RecyclerView.Adapter<CallHistoryAdapter.ViewHolder>() {
/*
    var departListOnItemClickListener: CallHistoryAdapterOnItemClickListener
*/
lateinit var sessionManager: SessionManager
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.call_history_recycler_layout, parent, false)
        )
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(department[position],position)

        holder.itemView.call_type.text=department[position].status
        holder.itemView.call_duration.text="minutes "+department[position].callMinutes



        if (department[position].senderId!=user_id){
            holder.itemView.Name.text=department[position].senderName
            holder.itemView.caller_type.text=""+department[position].senderType

            Glide.with(context).load(department[position].senderImage).centerCrop().placeholder(R.drawable.profile_placeholder).into(holder.itemView.profile);
            if (department[position].status.equals("missed")){
                holder.itemView.call_icon.setImageResource(R.drawable.ic_baseline_call_missed_24)
                holder.itemView.call_icon.setColorFilter(ContextCompat.getColor(context, R.color.gplus_color_4));

            }else if (department[position].status.equals("accepted")){
                holder.itemView.call_icon.setImageResource(R.drawable.ic_baseline_call_received_24)
                holder.itemView.call_icon.setColorFilter(ContextCompat.getColor(context, R.color.gplus_color_1));
            }
        }else{
            holder.itemView.Name.text=department[position].receiverName
            holder.itemView.caller_type.text=""+department[position].receiverType

            Glide
                .with(context)
                .load(department[position].receiverImage)
                .centerCrop()
                .placeholder(R.drawable.profile_placeholder)
                .into(holder.itemView.profile);
            if (department[position].status.equals("missed")){
                holder.itemView.call_icon.setImageResource(R.drawable.ic_baseline_call_missed_outgoing_24)
                holder.itemView.call_icon.setColorFilter(ContextCompat.getColor(context, R.color.gplus_color_4));

            }else if (department[position].status.equals("accepted")){
                holder.itemView.call_icon.setImageResource(R.drawable.ic_baseline_call_made_24)
                holder.itemView.call_icon.setColorFilter(ContextCompat.getColor(context, R.color.gplus_color_1));
            }
        }



        /*holder.itemView.departmentmainlay.setOnClickListener {
            departListOnItemClickListener!!.onItemClick(position,department[position].title)
        }*/

    }
    override fun getItemCount(): Int {
        return department.size
    }
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(version: CallHistoryItem,postion: Int) {}}
}
/*fun setOnItemClickListener(navigationOnClickListener: DepartListOnItemClickListener) {
    this.departListOnItemClickListener = navigationOnClickListener
}*/

interface CallHistoryAdapterOnItemClickListener {
    fun onItemClick(position: Int,dep:String)
}