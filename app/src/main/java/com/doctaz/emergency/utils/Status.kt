package com.doctaz.emergency

enum class Status { LOADING, SUCCESS, ERROR, COMPLETED, NOINTERNET }