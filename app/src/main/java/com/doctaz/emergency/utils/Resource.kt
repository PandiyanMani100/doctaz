package com.doctaz.emergency.utils

import com.doctaz.emergency.Status


//A generic class that holds a value with its loading status.
data class Resource<out T>(val status: Status, val data: T?, val message: String?) {
    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(Status.SUCCESS, data, null)
        }

        fun <T> error(msg: String, data: T?): Resource<T> {
            return Resource(Status.ERROR, data, msg)
        }

    /*    fun <T> nointernet(msg: String, data: T?): Resource<T> {
            return Resource(Status.NOINTERNET, data, msg)
        }


        fun <T> loading(data: T?): Resource<T> {
            return Resource(Status.LOADING, data, null)
        }*/
    }
}
