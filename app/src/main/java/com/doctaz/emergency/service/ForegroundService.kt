package com.doctaz.emergency.service


import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.AudioManager
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.text.Html
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.doctaz.emergency.R
import com.doctaz.emergency.constant.Iconstant
import com.doctaz.emergency.room.ChatDatabase
import com.doctaz.emergency.room.ChatTable
import com.doctaz.emergency.view.activity.CallAlert
import com.doctaz.emergency.view.activity.SessionManager
import com.doctaz.emergency.view.activity.SplashPage
import com.doctaz.emergency.viewmodel.VideoCallViewModel

import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.mindorks.kotnetworking.KotNetworking
import com.mindorks.kotnetworking.common.Priority
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.json.JSONException
import java.util.*
import kotlin.collections.ArrayList


class ForegroundService : Service() {
    private val mHandler = Handler()
    private var mTimer: Timer? = null
    var sessionManager: SessionManager? = null
    var reached: Int = 0
    var unreadChatList = ArrayList<ChatTable>()
    private lateinit var chatDatabase: ChatDatabase
    var notificationManager: NotificationManager? = null
    var channelId = ""
    private lateinit var videoCallViewModel: VideoCallViewModel

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        val input = intent.getStringExtra("inputExtra")
        sessionManager = SessionManager(this)
        createNotificationChannel()
        createNotificationChannel2()
        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle(resources.getString(R.string.app_name))
            .setContentText(resources.getString(R.string.welcome_to_doctor_consultation))
            .setSmallIcon(R.drawable.ic_launcherss)
            .build()
        startForeground(1, notification)
        if (mTimer != null) {
            mTimer!!.cancel()
            mTimer = null
        } else {
            mTimer = Timer() // recreate new timer
            mTimer!!.scheduleAtFixedRate(TimeDisplayTimerTask(), 0, INTERVAL)
        }
        return START_NOT_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mTimer != null) {
            mTimer!!.cancel()
            mTimer = null
        }
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(CHANNEL_ID, "Foreground Service Channel", NotificationManager.IMPORTANCE_DEFAULT)
            val manager = getSystemService(NotificationManager::class.java)
            manager.createNotificationChannel(serviceChannel)
        }
    }

    private fun createNotificationChannel2() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(CHANNEL_ID_Two, "channelName", NotificationManager.IMPORTANCE_DEFAULT)
            val manager = getSystemService(NotificationManager::class.java)
            manager.createNotificationChannel(serviceChannel)
        }
    }


    internal inner class TimeDisplayTimerTask : TimerTask() {
        @RequiresApi(Build.VERSION_CODES.O)
        override fun run() {
            // run on another thread
            mHandler.post {
                reached++
                if (reached > 4) {
                    val header = HashMap<String, String>()

                    header.put("receiver_id", sessionManager!!.getLoginId())
                    header.put("receiver_type", sessionManager!!.getLoginType())
                    reached = 0;


                    val cancelTag: String = "checkcall"
/*
                    KotNetworking.cancel(cancelTag)
*/
                    KotNetworking.post(Iconstant.baseUrl + "call/incoming_call")
                        .addBodyParameter(header)
                        .setPriority(Priority.HIGH)
                        .build()
                        .getAsJSONObject { response, error ->
                            if (error != null) {
                                Log.d("res--", error.toString())
                            } else {
                                Log.d("res--", response.toString())
                                if (response?.has("message")!!){

                                }else if (response?.has("room_token")!!){
                                    val bundle = Bundle()
                                    try {
                                        bundle.putString("sessionId", response!!.getString("room_number"))
                                        bundle.putString("sessionToken", response.getString("room_token"))
                                        bundle.putString("doctorName", response.getString("sender_name"))
                                        bundle.putString("apiKey", "47126264")
                                        bundle.putString("callType", resources.getString(R.string.firebase_call))

                                    } catch (e: JSONException) {
                                        e.printStackTrace()
                                    }
                                    val intent = Intent(applicationContext, CallAlert::class.java)
                                    intent.putExtras(bundle)
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                    startActivity(intent)

                                }





                            }
                        }

//                        {"receiver_id":"40","receiver_type":"patient","status":"pending","sender_id":"31","sender_type":"doctor","room_number":"2_MX40NzEyNjI2NH5-MTYxNDkyMTA4NjczM35vMXJKY1RXY2J4VENNS0RMQTJSTUh5UHR-fg","room_token":"T1==cGFydG5lcl9pZD00NzEyNjI2NCZzaWc9YWM3MDJmYzc0OWJkN2Y3Y2Q1MWIxYmRlYzQwZjY2YWY0MDVmOGRiOTpzZXNzaW9uX2lkPTJfTVg0ME56RXlOakkyTkg1LU1UWXhORGt5TVRBNE5qY3pNMzV2TVhKS1kxUlhZMko0VkVOTlMwUk1RVEpTVFVoNVVIUi1mZyZjcmVhdGVfdGltZT0xNjE0OTIxMDg2JnJvbGU9cHVibGlzaGVyJm5vbmNlPTE2MTQ5MjEwODYuOTEyMjE1MjE2MDc5ODcmZXhwaXJlX3RpbWU9MTYxNTUyNTg4NiZpbml0aWFsX2xheW91dF9jbGFzc19saXN0PQ==","sender_name":"Test
//                            Doctor"}




                    chatMessageAlert()
                }


            }
        }
    }


 /*   private fun addUser(email: String, mobile: String, message: String, roomNumber: String) {
        videoCallViewModel.getvideocallparams().observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> Toast.makeText(this, resources.getString(R.string.add_user_success_responce), Toast.LENGTH_SHORT).show()
                Status.ERROR -> Toast.makeText(this@PublishVideoCall, resources.getString(R.string.invalid), Toast.LENGTH_SHORT).show()
                else -> Toast.makeText(this@PublishVideoCall, resources.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show()
            }
        })
    }*/

    @RequiresApi(Build.VERSION_CODES.O)
    private fun chatMessageAlert() {
        val header = HashMap<String, String>()
        header.put("user_id", sessionManager!!.getLoginId())
        header.put("user_type", sessionManager!!.getLoginType())
        header.put("status", "unread")
        val cancelTag: String = "checkcall"
        KotNetworking.cancel(cancelTag)
        KotNetworking.post(Iconstant.baseUrl + "messages/allmessages")
            .addBodyParameter(header)
            .setTag(cancelTag)
            .setPriority(Priority.HIGH)
            .build()
            .getAsJSONArray() { response, error ->
                if (error != null) {
                    Log.d("res--", error.toString())
                } else {
                    unreadChatList.clear()
                    if (response != null && response.length() > 0) {
                        for (i in 0 until response.length()) {
                            val objects = response.getJSONObject(i)
                            val message_group_id = objects.getString("message_group_id")
                            val message_sender = objects.getString("message_sender")
                            val message_sender_type = objects.getString("message_sender_type")
                            val message_receiver = objects.getString("message_receiver")
                            val message_receiver_type = objects.getString("message_receiver_type")
                            val message_desc = objects.getString("message_desc")
                            val message_file = objects.getString("message_file")
                            val message_date = objects.getString("message_date")
                            val message_status = objects.getString("message_status")
                            val message_sender_name = objects.getString("message_sender_name")
                            val message_sender_image = objects.getString("message_sender_image")
                            val message_receiver_name = objects.getString("message_receiver_name")
                            val message_receiver_image = objects.getString("message_receiver_image")
                            unreadChatList.add(ChatTable(0, message_group_id, message_date, message_desc, message_file, message_receiver, message_receiver_image, message_receiver_name, message_receiver_type, message_sender, message_sender_image, message_sender_name, message_sender_type, message_status))
                        }

                        ShowCustomNotification( "New Message From ".plus(unreadChatList[unreadChatList.size - 1].messageSenderName),unreadChatList[unreadChatList.size - 1].messageDesc)


//                        showNotification(applicationContext, "New Message From ".plus(unreadChatList[unreadChatList.size - 1].messageSenderName), unreadChatList[unreadChatList.size - 1].messageDesc)
//                        ShowCustomNotification
                        chatDatabase = ChatDatabase.getChatDatabase(applicationContext)
                        CoroutineScope(Dispatchers.IO).launch {
                            chatDatabase.chatDao().addChat(unreadChatList)


                        }


                        EventBus.getDefault().post(unreadChatList)

                    }
                }
            }
    }


/*    private fun showNotification(context: Context, title: String?, body: String?) {
        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notificationId = 1000
        val channelId = "channel-01"
        val channelName = "Channel Name"
        val importance = NotificationManager.IMPORTANCE_HIGH
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val mChannel = NotificationChannel(channelId, channelName, importance)
            notificationManager.createNotificationChannel(mChannel)
        }
        val mBuilder = NotificationCompat.Builder(context, channelId)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
            .setContentTitle(title).setContentText(body)
            .setVibrate(longArrayOf(1000, 1000))
            .setDefaults(Notification.FLAG_ONLY_ALERT_ONCE)
        MediaPlayer.create(applicationContext, R.raw.incoming_message).start()
        notificationManager.notify(notificationId, mBuilder.build())
    }*/





/*@RequiresApi(Build.VERSION_CODES.O)
private fun showNotification(mContext: Context, message: String?, body: String?) {

    try {
        val serviceChannel = NotificationChannel(CHANNEL_ID_Two, "channelName", NotificationManager.IMPORTANCE_DEFAULT)
        val manager = getSystemService(NotificationManager::class.java)
        manager.createNotificationChannel(serviceChannel)
        val notificationId = 1001
        val channelName = "ChannelName"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) run {
            val mBuilder = NotificationCompat.Builder(mContext, CHANNEL_ID_Two)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
                .setContentTitle(message).setContentText(body)
                .setVibrate(longArrayOf(1000, 1000))
                .setDefaults(Notification.FLAG_ONLY_ALERT_ONCE)
            MediaPlayer.create(applicationContext, R.raw.incoming_message).start()
            manager.notify(notificationId, mBuilder.build())
        } else {

        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val mBuilder = NotificationCompat.Builder(mContext, CHANNEL_ID_Two)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
                .setContentTitle(message).setContentText(body)
                .setVibrate(longArrayOf(1000, 1000))
                .setDefaults(Notification.FLAG_ONLY_ALERT_ONCE)
            MediaPlayer.create(applicationContext, R.raw.incoming_message).start()
            manager.notify(notificationId, mBuilder.build())
        }

    } catch (e: Exception) {
        println("Notification_Exception--------" + e.toString())
    }
}*/

    private fun ShowCustomNotification(title: String, message: String) {

        try {

            var titles: CharSequence = title
            if (title == "") {
                titles = Html.fromHtml("<b>" + resources.getString(R.string.app_name) + "</b> ")
            } else {
                titles = Html.fromHtml("<b>$titles</b> ")
            }


            val contentIntent = PendingIntent.getActivity(applicationContext, 0, Intent(), 0)

            var soundUri: Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            soundUri = Uri.parse("android.resource://" + applicationContext.packageName + "/" + "")
            val manager: AudioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
            manager.setStreamVolume(AudioManager.STREAM_MUSIC, 100, 0)
            val nm = applicationContext!!.getSystemService(Service.NOTIFICATION_SERVICE) as NotificationManager
            val res = applicationContext!!.getResources()
            var n: Notification


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) run {
                val intent: Intent
            intent = Intent(this, SplashPage::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
            var soundUrinew = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            soundUrinew = Uri.parse("android.resource://" + applicationContext.packageName + "/" )
            val managernew = getSystemService(Context.AUDIO_SERVICE) as AudioManager
            managernew.setStreamVolume(AudioManager.STREAM_MUSIC, 100, 0)
            val notificationBuilder: NotificationCompat.Builder = NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(res, R.mipmap.ic_launcher))
                .setContentTitle(getString(R.string.app_name))
                .setContentText(message)
                .setAutoCancel(true)
                .setContentTitle(titles)

                .setSound(soundUrinew)
                .setContentIntent(pendingIntent)
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val mChannel = NotificationChannel(CHANNEL_ID, "test", NotificationManager.IMPORTANCE_HIGH)
                notificationManager.createNotificationChannel(mChannel)
            }
            notificationManager.notify(0, notificationBuilder.build())
            }
            else {
                val builder = NotificationCompat.Builder(applicationContext)
                builder.setContentIntent(contentIntent)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setColor(applicationContext!!.resources.getColor(R.color.appColor))
                    .setLargeIcon(BitmapFactory.decodeResource(res, R.mipmap.ic_launcher))
                    .setTicker(message)
                    .setWhen(System.currentTimeMillis())
                    .setAutoCancel(true)
                    .setStyle(NotificationCompat.BigTextStyle()
                        .bigText(message))
                    .setSound(soundUri)

                    .setContentTitle(titles)
                    .setLights(-0x10000, 100, 2000)
                    .setPriority(NotificationManager.IMPORTANCE_HIGH)
                    .setContentText(message)
                n = builder.notification
                n.flags = n.flags or Notification.FLAG_AUTO_CANCEL
                n.defaults = n.defaults or Notification.DEFAULT_ALL
                nm.notify(0,n )
            }














            val mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val builder = Notification.Builder(applicationContext)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) run {
         /*       val channelId = "0"
                val channelName = "chatChannel"
                val importance = NotificationManager.IMPORTANCE_HIGH*/

                val notificationChannel = NotificationChannel(channelId, "chatChannel", NotificationManager.IMPORTANCE_DEFAULT)

                mNotificationManager.createNotificationChannel(notificationChannel)

//                val notificationChannel = NotificationChannel(channelId, channelName, importance)

                nm.createNotificationChannel(notificationChannel)

                builder.setContentIntent(contentIntent)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setLargeIcon(BitmapFactory.decodeResource(res, R.mipmap.ic_launcher))
                    .setTicker(message)
                    .setWhen(System.currentTimeMillis())
                    .setColor(applicationContext!!.resources.getColor(R.color.appColor))
                    .setAutoCancel(false)
                    .setSound(soundUri)
                    .setStyle(Notification.BigTextStyle()
                        .bigText(message))
                    .setContentTitle(titles)
                    .setContentText(message)
                n = builder.notification

            } else {

                val builder = NotificationCompat.Builder(applicationContext)
                builder.setContentIntent(contentIntent)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setColor(applicationContext!!.resources.getColor(R.color.appColor))
                    .setLargeIcon(BitmapFactory.decodeResource(res, R.mipmap.ic_launcher))
                    .setTicker(message)
                    .setWhen(System.currentTimeMillis())
                    .setAutoCancel(true)
                    .setStyle(NotificationCompat.BigTextStyle()
                    .bigText(message))
                    .setSound(soundUri)

                    .setContentTitle(titles)
                    .setLights(-0x10000, 100, 2000)
                    .setPriority(NotificationManager.IMPORTANCE_HIGH)
                    .setContentText(message)
                n = builder.notification
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                val smallIconViewId = applicationContext!!.resources.getIdentifier("right_icon", "id", android.R::class.java.getPackage().name)
                if (smallIconViewId != 0) {
                    if (n.contentView != null)
                        n.contentView.setViewVisibility(smallIconViewId, View.INVISIBLE)

                    if (n.headsUpContentView != null)
                        n.headsUpContentView.setViewVisibility(smallIconViewId, View.INVISIBLE)

                    if (n.bigContentView != null)
                        n.bigContentView.setViewVisibility(smallIconViewId, View.INVISIBLE)
                }


            }else{
                n.flags = n.flags or Notification.FLAG_AUTO_CANCEL
                n.defaults = n.defaults or Notification.DEFAULT_ALL
                nm.notify(0,n )
                mNotificationManager.notify(0, builder.build());
            }




            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val smallIconViewId = applicationContext!!.resources.getIdentifier("right_icon", "id", android.R::class.java.getPackage().name)
                if (smallIconViewId != 0) {
                    if (n.contentView != null)
                        n.contentView.setViewVisibility(smallIconViewId, View.INVISIBLE)

                    if (n.headsUpContentView != null)
                        n.headsUpContentView.setViewVisibility(smallIconViewId, View.INVISIBLE)

                    if (n.bigContentView != null)
                        n.bigContentView.setViewVisibility(smallIconViewId, View.INVISIBLE)
                }
            }

            n.flags = n.flags or Notification.FLAG_AUTO_CANCEL
            n.defaults = n.defaults or Notification.DEFAULT_ALL
            nm.notify(0,n )
            mNotificationManager.notify(0, builder.build());

        } catch (e: Exception) {

            println("Notification_Exception--------" + e.toString())
        }

    }



/*
    private fun ShowCustomNotificationdf(mContext: Context, message: String, rideid: String) {
        try {
            var titles: CharSequence = title
            if (title == "") {
                titles = Html.fromHtml("<b>" + resources.getString(R.string.app_name) + "</b> ")
            } else {
                titles = Html.fromHtml("<b>$titles</b> ")
            }
            var notificationIntent: Intent? = null
            notificationIntent = Intent(mContext, ChatActivity::class.java)
            notificationIntent.putExtra("Ride_id", rideid)
            sessionManager!!.setfirebase("firebase")
//            notificationIntent.putExtra("fromfirebase", "ddfg")
            val contentIntent = PendingIntent.getActivity(mContext, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)
            val nm = mContext!!.getSystemService(Service.NOTIFICATION_SERVICE) as NotificationManager
            val res = mContext!!.getResources()
            val n: Notification
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) run {
                val channelId = "0"
                val channelName = "chatChannel"
                val importance = NotificationManager.IMPORTANCE_HIGH
                val notificationChannel = NotificationChannel(channelId, channelName, importance)
                nm.createNotificationChannel(notificationChannel)
                val builder = Notification.Builder(mContext, channelId)
                builder.setContentIntent(contentIntent)
                    .setSmallIcon(R.drawable.app_logo)
                    .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.app_logo))
                    .setTicker(message)
                    .setWhen(System.currentTimeMillis())
                    .setColor(mContext!!.resources.getColor(R.color.app_color))
                    .setAutoCancel(false)
                    .setStyle(Notification.BigTextStyle()
                        .bigText(message))
                    .setContentTitle(titles)
                    .setContentText(message)
                n = builder.notification
            } else {
                val builder = NotificationCompat.Builder(mContext)
                builder.setContentIntent(contentIntent)
                    .setSmallIcon(R.drawable.app_logo)
                    .setColor(mContext!!.resources.getColor(R.color.app_color))
                    .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.app_logo))
                    .setTicker(message)
                    .setWhen(System.currentTimeMillis())
                    .setAutoCancel(true)
                    .setStyle(NotificationCompat.BigTextStyle()
                        .bigText(message))
                    .setContentTitle(titles)
                    .setLights(-0x10000, 100, 2000)
                    .setPriority(NotificationManager.IMPORTANCE_HIGH)
                    .setContentText(message)
                n = builder.notification
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val smallIconViewId = mContext!!.resources.getIdentifier("right_icon", "id", android.R::class.java.getPackage().name)
                if (smallIconViewId != 0) {
                    if (n.contentView != null)
                        n.contentView.setViewVisibility(smallIconViewId, View.INVISIBLE)
                    if (n.headsUpContentView != null)
                        n.headsUpContentView.setViewVisibility(smallIconViewId, View.INVISIBLE)
                    if (n.bigContentView != null)
                        n.bigContentView.setViewVisibility(smallIconViewId, View.INVISIBLE)
                }
            }
            n.flags = n.flags or Notification.FLAG_AUTO_CANCEL
            n.defaults = n.defaults or Notification.DEFAULT_ALL
            nm.notify(0, n)
        } catch (e: Exception) {
            println("Notification_Exception--------" + e.toString())
        }
    }
*/











    companion object {
        const val CHANNEL_ID = "ForegroundServiceChannel"
        const val CHANNEL_ID_Two = "NotifiChannel"
        const val INTERVAL: Long = 1000
    }
}
