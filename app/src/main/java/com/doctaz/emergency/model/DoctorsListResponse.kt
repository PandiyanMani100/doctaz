package com.doctaz.emergency.model

import com.google.gson.annotations.SerializedName

class DoctorsListResponse : ArrayList<DoctorsListResponse.DoctorsList>() {
     class DoctorsList {
        @SerializedName("address")
        lateinit var address: String
        @SerializedName("approval_status")
        lateinit var approvalStatus: String
        @SerializedName("department_id")
        lateinit var departmentId: String
        @SerializedName("doctor_id")
        lateinit var doctorId: String
        @SerializedName("email")
        lateinit var email: String
        @SerializedName("last_active")
        lateinit var lastActive: String
        @SerializedName("medical_license")
        lateinit var medicalLicense: String
        @SerializedName("name")
        var name: String=""
        @SerializedName("online_status")
        lateinit var onlineStatus: String
        @SerializedName("password")
        lateinit var password: String
        @SerializedName("imageUrl")
        lateinit var imageUrl: String
        @SerializedName("phone")
        lateinit var phone: String
        @SerializedName("profile")
        lateinit var profile: String
        @SerializedName("registration_certification_number")
        lateinit var registrationCertificationNumber: String
        @SerializedName("social_links")
        lateinit var socialLinks: String
     }
}