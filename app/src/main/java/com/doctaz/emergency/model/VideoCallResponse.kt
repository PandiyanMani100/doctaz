package com.doctaz.emergency.model

import com.google.gson.annotations.SerializedName

class VideoCallResponse {
    @field:SerializedName("api_key")
    var apiKey: String = ""

    @field:SerializedName("sender_type")
    var senderType: String = ""

    @field:SerializedName("receiver_id")
    var receiverId: String = ""

    @field:SerializedName("room_token")
    var roomToken: String = ""

    @field:SerializedName("room_number")
    var roomNumber: String = ""

    @field:SerializedName("sender_id")
    var senderId: String = ""

    @field:SerializedName("receiver_type")
    var receiverType: String = ""

    @field:SerializedName("status")
    var status: String = ""
}
