package com.doctaz.emergency.model


import com.google.gson.annotations.SerializedName

class LoginResponse {
    @SerializedName("login_type")
    var loginType: String = ""

    @SerializedName("login_user_id")
    var loginUserId: String = ""

    @SerializedName("name")
    var name: String = ""

    @SerializedName("patient")
    lateinit var patient: Patient

    class Patient {
        @SerializedName("account_opening_timestamp")
        var accountOpeningTimestamp: Any = ""

        @SerializedName("address")
        var address: String = ""

        @SerializedName("age")
        var age: String = ""

        @SerializedName("approval_status")
        var approvalStatus: String = ""

        @SerializedName("birth_date")
        var birthDate: String = ""

        @SerializedName("blood_group")
        var bloodGroup: String = ""

        @SerializedName("code")
        var code: String = ""

        @SerializedName("device_token")
        var deviceToken: String = ""

        @SerializedName("email")
        var email: String = ""

        @SerializedName("healthcare_id")
        var healthcareId: String = ""

        @SerializedName("imageUrl")
        var imageUrl: String = ""

        @SerializedName("last_active")
        var lastActive: String = ""

        @SerializedName("name")
        var name: String = ""

        @SerializedName("online_status")
        var onlineStatus: Int = 0

        @SerializedName("password")
        var password: String = ""

        @SerializedName("patient_id")
        var patientId: String = ""

        @SerializedName("phone")
        var phone: String = ""

        @SerializedName("sex")
        var sex: String = ""
    }
}

