package com.doctaz.emergency.model


import com.google.gson.annotations.SerializedName

data class ProfileResponse(
    @SerializedName("account_opening_timestamp")
    val accountOpeningTimestamp: Any,
    @SerializedName("address")
    val address: String,
    @SerializedName("age")
    val age: String,
    @SerializedName("approval_status")
    val approvalStatus: String,
    @SerializedName("birth_date")
    val birthDate: String,
    @SerializedName("blood_group")
    val bloodGroup: String,
    @SerializedName("code")
    val code: String,
    @SerializedName("device_token")
    val deviceToken: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("healthcare_id")
    val healthcareId: String,
    @SerializedName("imageUrl")
    val imageUrl: String,
    @SerializedName("last_active")
    val lastActive: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("online_status")
    val onlineStatus: String,
    @SerializedName("password")
    val password: String,
    @SerializedName("patient_id")
    val patientId: String,
    @SerializedName("phone")
    val phone: String,
    @SerializedName("sex")
    val sex: String
)