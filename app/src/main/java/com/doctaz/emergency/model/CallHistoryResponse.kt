package com.doctaz.emergency.model

class CallHistoryResponse:ArrayList<Response>()
data class Response (val callMinutes: String? = null,
                     val callAmount: String? = null,
                     val senderType: String? = null,
                     val receiverId: String? = null,
                     val id: String? = null,
                     val senderId: String? = null,
                     val callId: String? = null,
                     val receiverType: String? = null,
                     val timestamp: String? = null,
                     val status: String? = null)
