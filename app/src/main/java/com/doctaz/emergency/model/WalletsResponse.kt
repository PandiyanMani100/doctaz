package com.doctaz.emergency.model

import com.google.gson.annotations.SerializedName

 class WalletsResponse :ArrayList<WalletResponseModelItem>()

data class WalletResponseModelItem(
    @SerializedName("current_balance")
    val currentBalance: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("month_earnings")
    val monthEarnings: String,
    @SerializedName("user_id")
    val userId: String,
    @SerializedName("user_type")
    val userType: String,
    @SerializedName("withdrawals")
    val withdrawals: String
)

