package com.doctaz.emergency.model


import com.google.gson.annotations.SerializedName

 class DoctorsTypeResponse: ArrayList<DoctorsTypeResponse.DoctorsType>() {
     class DoctorsType {
         @SerializedName("id")
         lateinit var id: String
         @SerializedName("name")
         lateinit var name: String
 }
 }





