package com.doctaz.emergency.model


import com.google.gson.annotations.SerializedName

 class CreateInboxGroupResponse{
     @SerializedName("message_group_id")
      var messageGroupId: String=""
 }
