package com.doctaz.emergency.model


import com.google.gson.annotations.SerializedName

data class DoctorRegistrationResponse(
    @SerializedName("address")
    val address: String,
    @SerializedName("approval_status")
    val approvalStatus: String,
    @SerializedName("department_id")
    val departmentId: Any,
    @SerializedName("doctor_id")
    val doctorId: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("healthcare_id")
    val healthcareId: Any,
    @SerializedName("imageUrl")
    val imageUrl: String,
    @SerializedName("last_active")
    val lastActive: String,
    @SerializedName("medical_license")
    val medicalLicense: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("online_status")
    val onlineStatus: String,
    @SerializedName("password")
    val password: String,
    @SerializedName("phone")
    val phone: String,
    @SerializedName("profile")
    val profile: String,
    @SerializedName("registration_certification_number")
    val registrationCertificationNumber: String,
    @SerializedName("social_links")
    val socialLinks: String
)