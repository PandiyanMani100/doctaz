package com.doctaz.emergency.model

import android.util.Patterns
import androidx.lifecycle.MutableLiveData

class LoginValidation(var mailAddress: String, var password: String) {

    fun getUserEmailAddress(): String {
        return mailAddress
    }

    fun getUserPassword(): String {
        return password
    }
    fun isEmailValid(): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(getUserEmailAddress()).matches()
    }
    fun isPasswordValid(): Boolean {
        return getUserPassword().length > 3
    }
}