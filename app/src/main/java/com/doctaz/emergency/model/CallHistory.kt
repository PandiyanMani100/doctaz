package com.doctaz.emergency.model


import com.google.gson.annotations.SerializedName

class CallHistory : ArrayList<CallHistoryItem>()

data class CallHistoryItem(
    @SerializedName("call_amount")
    val call_amount: String,
    @SerializedName("call_id")
    val callId: String,
    @SerializedName("call_minutes")
    val callMinutes: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("receiver_id")
    val receiverId: String,
    @SerializedName("receiver_type")
    val receiverType: String,
    @SerializedName("sender_id")
    val senderId: String,
    @SerializedName("sender_type")
    val senderType: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("timestamp")
    val timestamp: String,
    @SerializedName("receiverName")
val receiverName: String,
    @SerializedName("senderName")
    val senderName: String,
    @SerializedName("senderImage")
    val senderImage: String,
    @SerializedName("receiverImage")
    val receiverImage: String
)


