package com.doctaz.emergency.model

import com.google.gson.annotations.SerializedName

data class DashboardMainPojo(
    @SerializedName("department")
    val department: ArrayList<Department>
)
    data class Department(
        @SerializedName("offline")
        val offline: Int,
        @SerializedName("online")
        val online: Int,
        @SerializedName("title")
        val title: String,
        @SerializedName("total")
        val total: Int
    )
