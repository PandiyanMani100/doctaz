package com.doctaz.emergency.model

data class RecentMessageModel (val receiverName:String, val message:String, val date:String, val count:Int, val imageUri:String,val groupId:String,val receiverId:String,val receiverType:String)