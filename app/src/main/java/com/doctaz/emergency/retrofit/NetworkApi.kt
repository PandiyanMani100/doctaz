package com.doctaz.emergency.retrofit

import com.doctaz.emergency.model.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*
import java.util.HashMap

interface NetworkApi {

    @FormUrlEncoded 
    @POST("apis/login")
    fun getLogin(@FieldMap map: HashMap<String, String?>): Call<LoginResponse>

    @FormUrlEncoded
    @POST("call")
    fun getVideoCall(@FieldMap map: HashMap<String, String?>): Call<VideoCallResponse>

    @GET("apis/data/doctor")
    fun getDoctorsList(): Call<DoctorsListResponse>


    @GET("apis/data/doctor_types")
    fun getDoctorsType(): Call<DoctorsTypeResponse>


    @FormUrlEncoded
    @POST("apis/dashboard_details")
    fun getDepartmentcount(@FieldMap map: HashMap<String, String?>): Call<DashboardMainPojo>

    @FormUrlEncoded
    @POST("messages/insertMessage")
    fun sendMessage(@FieldMap map: HashMap<String, String?>): Call<ResponseBody>



    @FormUrlEncoded
    @POST("messages/createInboxGroup")
    fun createInboxGroup(@FieldMap map: HashMap<String, String?>): Call<CreateInboxGroupResponse>

    @FormUrlEncoded
    @POST("messages/allmessages")
    fun getChatHistory(@FieldMap map: HashMap<String, String?>): Call<RecentMessageResponse>

    @FormUrlEncoded
    @POST("call/accept_call")
    fun getAcceptCall(@FieldMap map: HashMap<String, String?>): Call<ResponseBody>

    @FormUrlEncoded
    @POST("call/add_users")
    fun getAddUsers(@FieldMap map: HashMap<String, String?>): Call<ResponseBody>

    @FormUrlEncoded
    @POST("apis/register/patient")
    fun registerPatient(@FieldMap map: HashMap<String, String?>): Call<PatientRegistrationResponse>

    @FormUrlEncoded
    @POST("apis/register/doctor")
    fun registerDoctor(@FieldMap map: HashMap<String, String?>): Call<DoctorRegistrationResponse>

    @FormUrlEncoded
    @POST("apis/register/nurse")
    fun registerNurse(@FieldMap map: HashMap<String, String?>): Call<NurseRegistrationResponse>

    @FormUrlEncoded
    @POST("apis/data/wallets")
    fun walletsdetails(@FieldMap map: HashMap<String, String?>): Call<WalletsResponse>
    @GET
    fun getProfileData(@Url url: String): Call<ProfileResponse>


    @FormUrlEncoded
    @POST("call/callHistory")
//    fun getcallhistoryData(@Query("sender_type") key:String,@Query("sender_id") key1:String): Call<CallHistory>
   fun getcallhistorysenderData(@FieldMap map: HashMap<String, String?>): Call<CallHistory>

    @FormUrlEncoded
    @POST("call/callHistory")
//    fun getcallhistoryData(@Query("sender_type") key:String,@Query("sender_id") key1:String): Call<CallHistory>
    fun getcallhistoryReceiverData(@FieldMap map: HashMap<String, String?>): Call<CallHistory>


    @FormUrlEncoded
    @POST("call/incoming_call")
    fun incomingCall(@FieldMap map: HashMap<String, String?>): Call<ResponseBody>



}