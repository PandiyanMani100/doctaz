@file:Suppress("DEPRECATION")

package com.doctaz.emergency.retrofit

import android.app.IntentService
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import android.util.Log
import com.doctaz.emergency.R
import com.doctaz.emergency.view.activity.SessionManager

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import org.greenrobot.eventbus.EventBus
import retrofit2.Response
import java.util.HashMap

class CommonAPIFetchService: IntentService("ApiHit") {

    private var disposable: CompositeDisposable? = null
    private lateinit var mSessionManager: SessionManager
    private var responseToSend: String = "failed"
    private var apiName: String = ""


    override fun onBind(intent: Intent): IBinder? {
        throw UnsupportedOperationException("Not yet implemented")
    }


    override fun onHandleIntent(intent: Intent?) {



        val apiService = RetrofitInstance.createService(NetworkApi::class.java)

        mSessionManager = SessionManager(applicationContext!!)
        if (intent?.hasExtra(getString(R.string.intent_putextra_api_key))!!) {
            apiName = intent.getStringExtra(getString(R.string.intent_putextra_api_key))!!
        }

        if (apiName == getString(R.string.Chat_Details_Api)) {


            val hashMaplist = intent.getSerializableExtra("items") as HashMap<String, String>
            Log.e("hashMaplist",""+hashMaplist)


         /*   disposable!!.add(apiService.getcallhistoryData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object :
                        DisposableSingleObserver<Response<CallHistoryResponse>>() {
                        override fun onSuccess(responseBody: Response<CallHistoryResponse>) {
                            if (responseBody.code() == 200) {
                                val nearbyserice = responseBody.body()!!
                                EventBus.getDefault().post(nearbyserice)

                       *//*         if (responseBody.body()!! == "1") {
                                    val nearbyserice = responseBody.body()!!
                                    Log.e("getprovidelist",""+nearbyserice.response)


                                    EventBus.getDefault().post(nearbyserice)
                                }else if (responseBody.body()!! == "2"){
                                    val nearbyserice = responseBody.body()!!
                                    Log.e("getprovidelist",""+nearbyserice)


                                    EventBus.getDefault().post(nearbyserice)
                                }
                                else {
                                    var errorresult =ErrorPojo(responseBody.body()!!,esponseBody.body()!!.status)
                                    EventBus.getDefault()
                                        .post(errorresult)
                                }*//*

                            } else {
                                EventBus.getDefault().post("failed,0")
                            }
                        }

                        override fun onError(e: Throwable) {
                            EventBus.getDefault().post(
                                getString(R.string.server_hit_fail)
                            )
                            Log.e("Error", "onError: " + e.message)
                        }


                    })
            )*/

        }

}

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        disposable = CompositeDisposable()
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onRebind(intent: Intent?) {
        super.onRebind(intent)
        disposable = CompositeDisposable()
    }

    override fun unbindService(conn: ServiceConnection) {
        super.unbindService(conn)
        disposable!!.dispose()
    }
}


