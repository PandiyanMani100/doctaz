package com.doctaz.emergency.retrofit

import com.doctaz.emergency.constant.Iconstant
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object RetrofitInstance {
    private var retrofit: Retrofit? = null
    private val okHttpClient = OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS).writeTimeout(60, TimeUnit.SECONDS).build()
    private val httpClient = OkHttpClient()

    private val builder = Retrofit.Builder()
        .baseUrl(Iconstant.baseUrl)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
    @Provides
    @Singleton
    fun getRetrofitInstance(): NetworkApi {
        if (retrofit == null) {
            retrofit = Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(Iconstant.baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
        return retrofit!!.create(NetworkApi::class.java)
    }

    fun <S> createService(serviceClass: Class<S>): S {
        val retrofit = builder.client(okHttpClient).build()
        return retrofit.create(serviceClass)
    }
}