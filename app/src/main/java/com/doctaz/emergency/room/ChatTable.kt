package com.doctaz.emergency.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "ChatTable")
data class ChatTable(

    @PrimaryKey(autoGenerate = true)
    @SerializedName("message_id")
    var messageId: Int,

    @SerializedName("message_group_id")
    var messageGroupId: String,

    @SerializedName("message_date")
    var messageDate: String,

    @SerializedName("message_desc")
    var messageDesc: String,

    @SerializedName("message_file")
    var messageFile: String,

    @SerializedName("message_receiver")
    var messageReceiver: String,

    @SerializedName("message_receiver_image")
    var messageReceiverImage: String,

    @SerializedName("message_receiver_name")
    var messageReceiverName: String="",

    @SerializedName("message_receiver_type")
    var messageReceiverType: String,

    @SerializedName("message_sender")
    var messageSender: String,

    @SerializedName("message_sender_image")
    var messageSenderImage: String,

    @SerializedName( "message_sender_name")
    var messageSenderName: String,

    @SerializedName("message_sender_type")
    var messageSenderType: String,

    @SerializedName("message_status")
    var messageStatus: String
)