package com.doctaz.emergency.room

import android.content.Context
import androidx.room.*

@Database(entities = [ChatTable::class], version = 1, exportSchema = false)
abstract class ChatDatabase : RoomDatabase() {

    abstract fun chatDao() : ChatDao

    companion object {

        @Volatile
        private var instance: ChatDatabase?=null

        fun getChatDatabase(context: Context) : ChatDatabase {
            if (instance != null) return instance!!
            instance = Room.databaseBuilder(context, ChatDatabase::class.java, "CHAT_DATABASE").fallbackToDestructiveMigration().build()
            return instance!!
        }

    }

}