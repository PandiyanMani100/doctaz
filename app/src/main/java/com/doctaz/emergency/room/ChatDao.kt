package com.doctaz.emergency.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface ChatDao {

    @Query("Select * from ChatTable")
    fun getAllChatList(): List<ChatTable>

    @Query("Select * from ChatTable where messageGroupId=:groupId")
    fun getUniqueChatList(groupId:String):List<ChatTable>

    @Query("Select DISTINCT messageGroupId from ChatTable")
    fun getUniqueGroups():List<String>

    @Query("Select * from ChatTable where messageGroupId=:id ORDER BY messageId DESC LIMIT 1")
    fun getLastReadChat(id:String):List<ChatTable>

    @Query("UPDATE ChatTable SET messageStatus = 'read' WHERE messageGroupId=:groupId")
    fun makeChatReadStatus(groupId:String)

    @Query("Select count(*) as count from ChatTable where messageGroupId=:id AND messageStatus=:messageStatus")
    fun getUnReadMessageCount(id:String,messageStatus:String):Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addChat(chatTable: List<ChatTable>)

    @Query("DELETE FROM ChatTable")
    fun deleteChatTable()


}