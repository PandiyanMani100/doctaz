@Parcelize
data class LoginResponse(
	val loginType: String? = null,
	val patient: Patient? = null,
	val name: String? = null,
	val loginUserId: String? = null
) : Parcelable

@Parcelize
data class Patient(
	val code: String? = null,
	val address: String? = null,
	val onlineStatus: Int? = null,
	val sex: String? = null,
	val birthDate: String? = null,
	val accountOpeningTimestamp: Any? = null,
	val password: String? = null,
	val phone: String? = null,
	val patientId: String? = null,
	val name: String? = null,
	val bloodGroup: String? = null,
	val lastActive: String? = null,
	val email: String? = null,
	val age: String? = null
) : Parcelable

